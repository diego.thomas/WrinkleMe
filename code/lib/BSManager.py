#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 10 11:06:51 2017

@author: diegothomas
"""
import imp
from os import path
import numpy as np
from numpy import linalg as LA
from math import sqrt, acos, cos, sin
import cv2
import threading
from multiprocessing import Process
import scipy.misc as ms
import scipy.sparse as sp
from scipy.sparse.linalg import spsolve
import pyopencl as cl
import pyopencl.array
from pyopencl.elementwise import ElementwiseKernel


from skimage.segmentation import slic
from skimage.segmentation import mark_boundaries
from skimage.util import img_as_float
import matplotlib.pyplot as plt

APP_ROOT = path.dirname( path.abspath( __file__ ) )
KernelsOpenCL = imp.load_source('BSKernels', APP_ROOT + '/BSKernels.py')


FACIAL_LANDMARKS = [3749, 3745, 3731, 3725, 3704, 1572, 1592, 1598, 1612, 1617, 3662, 
                    2207, 3093, 966, 2650, 2774, 2693, 662, 558, 2345, 2336, 2369, 
                    4266, 2237, 2298, 2096, 1981, 1980, 1978, 237, 181, 2838, 4307, 
                    4304, 4301, 4298, 4295, 732, 996, 998, 4277, 3148, 3133, 2865, 
                    2728, 2188,  629, 733, 1011, 3114, 3110]
'''913, 921, .., 3024, 3016'''

BackIndices = [174, 175, 1063, 1064, 1065, 1062, 1066, 1067, 1068, 1069, 1070, 1075, 1076, 1074, 1077, 1078, 
               1079, 1106, 1107, 1104, 1103, 1108, 1110, 1145, 1146, 1147, 1148, 1134, 1149, 1133, 1150, 1151, 
               1152, 1153, 1154, 1156, 1157, 1158, 1155, 1159, 1160, 1161, 1162, 1163, 1164, 1165, 1166, 1167, 
               1168, 1169, 1170, 1171, 1172, 1173, 1174, 1175, 1176, 1185, 1183, 1184, 1186, 1187, 1188, 1206, 
               1207, 1208, 1205, 1209, 1210, 1212, 1213, 1214, 1211, 1215, 1216, 1217, 1218, 1219, 1220, 1221, 
               1222, 1224, 1225, 1226, 1223, 1227, 1228, 2198, 3344, 3345, 1229, 1230, 1231, 2199, 1232, 1233, 
               1234, 1235, 2200, 1236, 1237, 1238, 1239, 1240, 1241, 1242, 1243, 1244, 1245, 1247, 1248, 1249, 
               1246, 1250, 1251, 1252, 1253, 1254, 1255, 1256, 1257, 1258, 1259, 1260, 1261, 1262, 1263, 1264, 
               1265, 1266, 1267, 2201, 3384, 1268, 1269, 3387, 3388, 1270, 1271, 1272, 1273, 3393, 2202, 1274, 
               1275, 1276, 1277, 1278, 1279, 1280, 1281, 1282, 1283, 1284, 1285, 1286, 1287, 1288, 1289, 1699, 
               1700, 1701, 1702, 1703, 1704, 1705, 1706, 1707, 1708, 1709, 1710, 1711, 1712, 1698, 1713, 1726, 
               1727, 1728, 1729, 1730, 1731, 1732, 1733, 1734, 1735, 3875, 3876, 1696, 1736, 1737, 1738, 3880, 
               3881, 177, 1739, 1740, 3884, 1697, 1741, 3894, 1749, 1750, 1751, 176, 1752, 1753, 2223, 2224, 2217, 
               2214, 3183, 3182, 3185, 3184, 3186, 3187, 3189, 3188, 3190, 3195, 3196, 3194, 3197, 3199, 3198, 3225, 
               3226, 3228, 3229, 3230, 3232, 3231, 3265, 3254, 3266, 3253, 3267, 3269, 3268, 3271, 3270, 3273, 3272, 
               3275, 3274, 3277, 3276, 3278, 3280, 3279, 3281, 3283, 3282, 3284, 3285, 3286, 3289, 3288, 3287, 3291, 
               3290, 3293, 3292, 3299, 3298, 3301, 3300, 3302, 3304, 3303, 3305, 3321, 3320, 3323, 3322, 3324, 3325, 
               3327, 3326, 3329, 3328, 3330, 3331, 3332, 3335, 3334, 3333, 3336, 3337, 3339, 3338, 3341, 3340, 3342, 
               3343, 3347, 3346, 3349, 3348, 3209, 3350, 3351, 3208, 3352, 3353, 3354, 3355, 3356, 3359, 3358, 3357, 
               3361, 3360, 3363, 3362, 3365, 3364, 3366, 3368, 3367, 3370, 3369, 3371, 3372, 3373, 3374, 3375, 3376, 
               3377, 3378, 3379, 3380, 3381, 3382, 3383, 3385, 3386, 3389, 3392, 3391, 3390, 3394, 3395, 3396, 3399, 
               3398, 3397, 3402, 3401, 3400, 3403, 3405, 3404, 3407, 3406, 3834, 3837, 3836, 3835, 3839, 3838, 3840, 
               3841, 3842, 3843, 3844, 3845, 3846, 3847, 3833, 3848, 3866, 3865, 3868, 3867, 3870, 3869, 3872, 3871, 
               3874, 3873, 3831, 3879, 3878, 3877, 3882, 3883, 3832, 3885, 3893, 3895, 3896, 3897]

TextureBridge = [(166,468), 
                 (45,347), 
                 (129, 304), 
                 (24, 201), 
                 (28, 301), 
                 (27, 404), 
                 (24, 405), 
                 (32, 403), 
                 (35, 403), 
                 (121, 403), 
                 (174, 401), 
                 (173, 402), 
                 (18, 324), 
                 (127, 324), 
                 (11, 227), 
                 (19, 227), 
                 (12, 227), 
                 (92, 214)]

def quaternion2matrix(q):
     m = np.identity(3)
     q00 = q[0] * q[0]
     q11 = q[1] * q[1]
     q22 = q[2] * q[2]
     q33 = q[3] * q[3]
     q03 = q[0] * q[3]
     q13 = q[1] * q[3]
     q23 = q[2] * q[3]
     q02 = q[0] * q[2]
     q12 = q[1] * q[2]
     q01 = q[0] * q[1]
     m[0][0] = q00 + q11 - q22 - q33
     m[1][1] = q00 - q11 + q22 - q33
     m[2][2] = q00 - q11 - q22 + q33
     m[0][1] = 2.0*(q12 - q03)
     m[1][0] = 2.0*(q12 + q03)
     m[0][2] = 2.0*(q13 + q02)
     m[2][0] = 2.0*(q13 - q02)
     m[1][2] = 2.0*(q23 - q01)
     m[2][1] = 2.0*(q23 + q01)
     
     return m


def Exponential(qsi):
    theta = LA.norm(qsi[3:6])
    res = np.identity(4)
    
    if (theta != 0.):
        res[0,0] = 1.0 + sin(theta)/theta*0.0 + (1.0 - cos(theta)) / (theta*theta) * (-qsi[5]*qsi[5] - qsi[4]*qsi[4])
        res[1,0] = 0.0 + sin(theta)/theta*qsi[5] + (1.0 - cos(theta))/(theta*theta) * (qsi[3]*qsi[4])
        res[2,0] = 0.0 - sin(theta)/theta*qsi[4] + (1.0 - cos(theta))/(theta*theta) * (qsi[3]*qsi[5])
        
        res[0,1] = 0.0 - sin(theta)/theta*qsi[5] + (1.0 - cos(theta))/(theta*theta) * (qsi[3]*qsi[4])
        res[1,1] = 1.0 + sin(theta) / theta*0.0 + (1.0 - cos(theta))/(theta*theta) * (-qsi[5]*qsi[5] - qsi[3]*qsi[3])
        res[2,1] = 0.0 + sin(theta)/theta*qsi[3] + (1.0 - cos(theta))/(theta*theta) * (qsi[4]*qsi[5])
        
        res[0,2] = 0.0 + sin(theta) / theta*qsi[4] + (1.0 - cos(theta))/(theta*theta) * (qsi[3]*qsi[5])
        res[1,2] = 0.0 - sin(theta)/theta*qsi[3] + (1.0 - cos(theta))/(theta*theta) * (qsi[4]*qsi[5])
        res[2,2] = 1.0 + sin(theta)/theta*0.0 + (1.0 - cos(theta))/(theta*theta) * (-qsi[4]*qsi[4] - qsi[3]*qsi[3])
        
        skew = np.zeros((3,3), np.float32)
        skew[0,1] = -qsi[5]
        skew[0,2] = qsi[4]
        skew[1,0] = qsi[5]
        skew[1,2] = -qsi[3]
        skew[2,0] = -qsi[4]
        skew[2,1] = qsi[3]
        
        V = np.identity(3) + ((1.0-cos(theta))/(theta*theta))*skew + ((theta - sin(theta))/(theta*theta))*np.dot(skew,skew)
        
        res[0,3] = V[0,0]*qsi[0] + V[0,1]*qsi[1] + V[0,2]*qsi[2]
        res[1,3] = V[1,0]*qsi[0] + V[1,1]*qsi[1] + V[1,2]*qsi[2]
        res[2,3] = V[2,0]*qsi[0] + V[2,1]*qsi[1] + V[2,2]*qsi[2]
    else:
        res[0,3] = qsi[0]
        res[1,3] = qsi[1]
        res[2,3] = qsi[2]
        
    return res

def Logarithm(Mat):
    trace = Mat[0,0]+Mat[1,1]+Mat[2,2]
    theta = acos((trace-1.0)/2.0)
    
    qsi = np.array([0.,0.,0.,0.,0.,0.])
    if (theta == 0.):
        qsi[3] = qsi[4] = qsi[5] = 0.0
        qsi[0] = Mat[0,3]
        qsi[1] = Mat[1,3]
        qsi[2] = Mat[2,3]
        return qsi
    
    R = Mat[0:3,0:3]
    lnR = (theta/(2.0*sin(theta))) * (R-np.transpose(R))
    
    qsi[3] = (lnR[2,1] - lnR[1,2])/2.0
    qsi[4] = (lnR[0,2] - lnR[2,0])/2.0
    qsi[5] = (lnR[1,0] - lnR[0,1])/2.0
    
    theta = LA.norm(qsi[3:6])

    skew = np.zeros((3,3), np.float32)
    skew[0,1] = -qsi[5]
    skew[0,2] = qsi[4]
    skew[1,0] = qsi[5]
    skew[1,2] = -qsi[3]
    skew[2,0] = -qsi[4]
    skew[2,1] = qsi[3]
    
    V = np.identity(3) + ((1.0 - cos(theta))/(theta*theta))*skew + ((theta-sin(theta))/(theta*theta))*np.dot(skew,skew)
    V_inv = LA.inv(V)
    
    qsi[0] = V_inv[0,0]*Mat[0,3] + V_inv[0,1]*Mat[1,3] + V_inv[0,2]*Mat[2,3]
    qsi[1] = V_inv[1,0]*Mat[0,3] + V_inv[1,1]*Mat[1,3] + V_inv[1,2]*Mat[2,3]
    qsi[2] = V_inv[2,0]*Mat[0,3] + V_inv[2,1]*Mat[1,3] + V_inv[2,2]*Mat[2,3]
    
    return qsi


'''
    The class to manipulate 3D .obj mesh
'''
class MyMesh():
    
    def __init__(self, filename):
        self.filename = filename
        
        
    def LoadMesh(self, adj = False):
        print "Loading ", self.filename
        
        f = open(self.filename, 'rb') 
        
        idx_v = 0
        idx_n = 0
        idx_uv = 0
        idx_f = 0
        for line in f:
            words = line.split()
            if (len(words) < 2):
                continue
            
            if (words[0] == "#"):
                if (words[1] == "Vertices:"):
                    self.Vertices = np.zeros((int(words[2]), 6), dtype = np.float32)
                    self.Normales = np.zeros((int(words[2]), 3), dtype = np.float32)
                    self.Rotations = np.zeros((int(words[2]), 3, 3), dtype = np.float32)
                elif (words[1] == "Faces:"):
                    self.Faces = np.zeros((int(words[2]), 6), dtype = np.int32)
                elif (words[1] == "Uvs:"):
                    self.UV = np.zeros((int(words[2]), 2), dtype = np.float32)
                continue
            
            if (words[0] == "v"):
                self.Vertices[idx_v] = [float(words[1]), float(words[2]), float(words[3]), idx_v, 0, 0]
                idx_v+=1
            
            if (words[0] == "vn"):
                self.Normales[idx_n] = [float(words[1]), float(words[2]), float(words[3])]
                norm = sqrt(float(words[1])*float(words[1]) + float(words[2])*float(words[2]) + float(words[3])*float(words[3])) #LA.norm(self.Normales[idx_n])
                if (norm != 0.0):
                    self.Normales[idx_n] = self.Normales[idx_n]/norm
                idx_n+=1
            
            if (words[0] == "vt"):
                self.UV[idx_uv] = [words[1], words[2]]
                idx_uv+=1
                
            if (words[0] == "f"):
                self.Faces[idx_f] = np.array([words[1].split("/"), words[2].split("/"), words[3].split("/")]).reshape(6)
                self.Faces[idx_f] = self.Faces[idx_f] - 1
                
                self.Vertices[self.Faces[idx_f,0],4:6] = self.UV[self.Faces[idx_f,1],:]
                self.Vertices[self.Faces[idx_f,2],4:6] = self.UV[self.Faces[idx_f,3],:]
                self.Vertices[self.Faces[idx_f,4],4:6] = self.UV[self.Faces[idx_f,5],:]
                
                idx_f+=1

        f.close()
        
        # Compute adjacent faces
        if (adj):
            self.AdjFaces = []
            idx = 0
            nb_faces = self.Faces.shape[0]
            for face in self.Faces:
                adj_list = []
                for curr_idx in range(idx, nb_faces):
                    curr = self.Faces[curr_idx,::]
                    count = 0
                    if (face[0] == curr[0] or face[0] == curr[2] or face[0] == curr[4]):
                        count +=1
                    if (face[2] == curr[0] or face[2] == curr[2] or face[2] == curr[4]):
                        count +=1
                    if (face[4] == curr[0] or face[4] == curr[2] or face[4] == curr[4]):
                        count +=1
                    if (count==2):
                        adj_list.append(curr)
                    if (len(adj_list)==3):
                        break 
                
                for curr_indx in range(idx, 0, -1):
                    curr = self.Faces[curr_indx,::]
                    count = 0
                    if (face[0] == curr[0] or face[0] == curr[2] or face[0] == curr[4]):
                        count +=1
                    if (face[2] == curr[0] or face[2] == curr[2] or face[2] == curr[4]):
                        count +=1
                    if (face[4] == curr[0] or face[4] == curr[2] or face[4] == curr[4]):
                        count +=1
                    if (count==2):
                        adj_list.append(curr)
                    if (len(adj_list)==3):
                        break 
                self.AdjFaces.append(adj_list)
                print idx
                idx +=1
        
        # set back point values to true
        self.IsBack = np.zeros(self.Vertices.shape[0], dtype = np.bool)
        self.IsBack[BackIndices] = True
        
    def SaveMesh(self):
        print "Saving ", self.filename
        
        ''' Write result 3D mesh into a .ply file'''
        f = open("../../../Results/WrinkleMe/Neutral.ply", 'wb')
        
        # Write headers
        f.write("ply\n")
        f.write("format ascii 1.0\n")
        f.write("comment ply file created by Diego Thomas\n")
        f.write("element vertex %d \n" %(self.Vertices.shape[0]))
        f.write("property float x\n")
        f.write("property float y\n")
        f.write("property float z\n")
        f.write("property float nx\n")
        f.write("property float ny\n")
        f.write("property float nz\n")
        f.write("element face %d \n" %(self.Faces.shape[0]))
        f.write("property list uchar int vertex_indices\n")
        f.write("end_header\n")
        
        
        for (v,n) in zip(self.Vertices, self.Normales):
            f.write("%f %f %f %f %f %f\n" %(v[0], v[1], v[2], 
                                                n[0], n[1], n[2]))
            
        for face_curr in self.Faces:
            f.write("3 %d %d %d \n" %(face_curr[0], face_curr[2], face_curr[4])) 
        
        f.close()
    
        
    '''
        Function to draw the mesh using tkinter
    '''
    def DrawMesh(self, Pose, intrinsic, Size, canvas):
        
        #Draw all faces
        nb_faces, _ = self.Faces.shape
        pix = np.array([0., 0., 1.])
        pt = np.array([0., 0., 0., 1.])
        for i in range(nb_faces):
            inviewingvolume = False
            poly = []
            nmle = np.zeros(3, np.float32)
            for k in range(3):
                nmle[0] += self.Normales[self.Faces[i,2*k],0]
                nmle[1] += self.Normales[self.Faces[i,2*k],1]
                nmle[2] += self.Normales[self.Faces[i,2*k],2]
                nmle = np.dot(Pose[0:3,0:3], nmle)
                pt[0] = self.Vertices[self.Faces[i,2*k],0]
                pt[1] = self.Vertices[self.Faces[i,2*k],1]
                pt[2] = self.Vertices[self.Faces[i,2*k],2]
                pt = np.dot(Pose, pt)
                pix[0] = pt[0]/pt[2]
                pix[1] = pt[1]/pt[2]
                pix = np.dot(intrinsic, pix)
                column_index = int(round(pix[0]))
                line_index = int(round(pix[1]))
                poly.append((column_index, line_index))
                    
                if (column_index > -1 and column_index < Size[1] and line_index > -1 and line_index < Size[0] and pt[2] > 0. and nmle[2] < 0.):
                    inviewingvolume = True
        
            norm_nmle = sqrt(nmle[0]*nmle[0] + nmle[1]*nmle[1] + nmle[2]*nmle[2])
            nmle = (nmle/norm_nmle + 1.0)*126.0
            r = str(hex(int(nmle[0])))[2::] 
            if (len(r) == 1):
                r = '0'+r
            g = str(hex(int(nmle[1])))[2::] 
            if (len(g) == 1):
                g = '0'+g
            b = str(hex(int(nmle[2])))[2::] 
            if (len(b) == 1):
                b = '0'+ b
            arg = '#' + r + g + b
            if inviewingvolume:
                canvas.create_polygon(*poly, fill=arg)
                
                
    def ComputeTgtPlane(self):
        
        for face in self.Faces:
        #for pt1, nmle1 in zip(self.Vertices, self.Normales):
            for k in range(3):
                pt_idx = face[2*k]
                norm = self.Rotations[pt_idx][0,0] + self.Rotations[pt_idx][0,1] + self.Rotations[pt_idx][0,2] + self.Rotations[pt_idx][1,0] + self.Rotations[pt_idx][1,1] + self.Rotations[pt_idx][1,2] + self.Rotations[pt_idx][2,0] + self.Rotations[pt_idx][2,1] + self.Rotations[pt_idx][2,2]
                if (not norm == 0.0):
                    continue
                
                # Compute original and transformed tangent basis
                pt1 = self.Vertices[pt_idx][0:3]
                e3 = self.Normales[pt_idx]
                
                # Compute original basis, centered on pt1, with z nmle and oriented in p1->p2
                pt2 = self.Vertices[face[2*((k+1)%3)]][0:3]
                e1 = pt2-pt1
                proj = np.dot(e3, e1)
                e1 = e1 - proj*e3
                norm_e1 = sqrt(e1[0]*e1[0] + e1[1]*e1[1] + e1[2]*e1[2])
                if (not norm_e1 == 0.):
                    e1 = e1/norm_e1
            
                e2 = np.cross(e3, e1)
                norm_e2 = sqrt(e2[0]*e2[0] + e2[1]*e2[1] + e2[2]*e2[2])
                if (not norm_e2 == 0.):
                    e2 = e2/norm_e2
            
                # Compute transformed basis, centered on pt1', with z nmle and oriented in p1'->p2'
                Tpt1 = self.T_Vertices[pt_idx][0:3]
                Tpt2 = self.T_Vertices[face[2*((k+1)%3)]][0:3]
                Te3 = self.T_Normales[pt_idx]
                Te1 = Tpt2 - Tpt1
                proj = np.dot(Te1, Te3)
                Te1 = Te1 - proj*Te3
                norm_Te1 = sqrt(Te1[0]*Te1[0] + Te1[1]*Te1[1] + Te1[2]*Te1[2])
                if (not norm_Te1 == 0.):
                    Te1 = Te1/norm_Te1
                
                Te2 = np.cross(Te3, Te1)
                norm_Te2 = sqrt(Te2[0]*Te2[0] + Te2[1]*Te2[1] + Te2[2]*Te2[2])
                if (not norm_Te2 == 0.):
                    Te2 = Te2/norm_Te2
                
                # Compute Rotation matrix
                B1 = np.array([e1.T, e2.T, e3.T])
                TB1 = np.array([Te1.T, Te2.T, Te3.T])
                
                Rot = np.dot(TB1, LA.inv(B1))
                
                self.Rotations[pt_idx] = Rot
    
    def ComputeTgtPlaneInner(self, InnerFaces, FixedVtx, InnerVtx):
        
        self.Rotations = np.zeros((len(FixedVtx)+len(InnerVtx), 3, 3), dtype = np.float32)
        
        for face in InnerFaces:
        #for pt1, nmle1 in zip(self.Vertices, self.Normales):
            for k in range(3):
                pt_idx = face[2*k+1]
                if (not LA.norm(self.Rotations[face[2*k]]) == 0.0):
                    continue
                
                # Compute original and transformed tangent basis
                pt1 = self.Vertices[pt_idx][0:3]
                e3 = self.Normales[pt_idx]
                
                # Compute original basis, centered on pt1, with z nmle and oriented in p1->p2
                pt2 = self.Vertices[face[2*((k+1)%3)+1]][0:3]
                e1 = pt2-pt1
                proj = np.dot(e3, e1)
                e1 = e1 - proj*e3
                if (not LA.norm(e1) == 0.):
                    e1 = e1/LA.norm(e1)
            
                e2 = np.cross(e3, e1)
                if (not LA.norm(e2) == 0.):
                    e2 = e2/LA.norm(e2)
            
                # Compute transformed basis, centered on pt1', with z nmle and oriented in p1'->p2'
                Tpt1 = self.T_Vertices[pt_idx][0:3]
                Tpt2 = self.T_Vertices[face[2*((k+1)%3)+1]][0:3]
                Te3 = self.T_Normales[pt_idx]
                Te1 = Tpt2 - Tpt1
                proj = np.dot(Te1, Te3)
                Te1 = Te1 - proj*Te3
                if (not LA.norm(Te1) == 0.):
                    Te1 = Te1/LA.norm(Te1)
                
                Te2 = np.cross(Te3, Te1)
                if (not LA.norm(Te2) == 0.):
                    Te2 = Te2/LA.norm(Te2)
                
                # Compute Rotation matrix
                B1 = np.array([e1.T, e2.T, e3.T])
                TB1 = np.array([Te1.T, Te2.T, Te3.T])
                
                Rot = np.dot(TB1, LA.inv(B1))
                
                self.Rotations[face[2*k]] = Rot
                
    def GetWeightedNormal(self, face):
        v1 = self.Vertices[face[2]][0:3] - self.Vertices[face[0]][0:3]
        v2 = self.Vertices[face[4]][0:3] - self.Vertices[face[0]][0:3]
        
        b = v1 / LA.norm(v1)
        
        proj = np.dot(b,v2)
        
        h = v2 - proj*b
        hauteur = sqrt(np.dot(h,h))
        area = LA.norm(v1)*hauteur/2.
        
        tmp = np.cross(v1,v2)
        
        res = (tmp/LA.norm(tmp))*area
        
        return res
        

'''
    The main class to manipulate blendshapes
'''
class BSMng():
    
    ''' Constructor '''
    def __init__(self, GPUManager, path, D2RGB):
        self.path = path
        self.nbBS = 28
        self.BlendShapes = []
        self.Size = (400,400,3)
        self.SizeDraw = (480, 640, 3)
        self.GPUManager = GPUManager
        self.indexSave = 0
        
        
        '''Label_img = np.zeros((800,800), dtype = np.uint16)
        Weight_img = np.zeros((800,800, 3), dtype = np.uint16)
        for i in range(8):
            for j in range(8):
                i_low = 100*i
                i_up = 100*(i+1)
                j_low = 100*j
                j_up = 100*(j+1)
                img0 = cv2.imread("Weight_img"+str(i_low)+str(i_up)+str(j_low)+str(j_up)+".png", cv2.IMREAD_UNCHANGED)
                img1 = cv2.imread("Label_img"+str(i_low)+str(i_up)+str(j_low)+str(j_up)+".png", cv2.IMREAD_UNCHANGED)
                Weight_img[i_low:i_up, j_low:j_up, :] = img0[i_low:i_up, j_low:j_up, :]
                Label_img[i_low:i_up, j_low:j_up] = img1[i_low:i_up, j_low:j_up]
        
        cv2.imwrite("Label_img.png", Label_img)
        cv2.imwrite("Weight_img.png", Weight_img)'''
        
        
        img0 = cv2.imread("../data/Weights-400.png", cv2.IMREAD_UNCHANGED)
        img1 = cv2.imread("../data/Labels-400.png", cv2.IMREAD_UNCHANGED)
        #img2 = cv2.imread("../data/FrontFace.png", cv2.IMREAD_UNCHANGED)
        img3 = cv2.imread("../data/Labelsb-240.png", cv2.IMREAD_UNCHANGED)
        img4 = cv2.imread("../data/LabelsMask.bmp", cv2.IMREAD_UNCHANGED)
        img3 = ms.imresize(img3, self.Size)
        
        self.Bump = np.zeros(self.Size, dtype = np.float32)
        self.Bump[:,:,2] = np.ascontiguousarray(img1[:,:].astype(np.float32) -1.0)
        self.Bump[108:133, 128,2] = -1.0
        self.Bump[img3[:,:,2] > 100,1] = -1.0
        self.Bump[img1[:,:] == 0,1] = 0.0
        self.labels = np.zeros((self.Size[0], self.Size[1]), dtype = np.int8)
        #self.labels = np.ascontiguousarray(img2[:,:,2].astype(np.float32)>100).astype(np.int8)
        self.RGB = np.zeros(self.Size, dtype = np.float32)
        self.WeightMap = np.ascontiguousarray(img0.astype(np.float32) /65535.0)
        self.Vertices = np.zeros((28*4325,3), dtype = np.float32)
        self.Normales = np.zeros((28*4325,3), dtype = np.float32)
        self.BlendshapeCoeff = np.zeros(28, dtype = np.float32)
        
        
        self.Bump_Mapping = ElementwiseKernel(self.GPUManager.context, 
                                               """float *Bump, float *RGBMapBump, float *BumpSwap, float *RGBMapBumpSwap, 
                                               float *VMapBump, float *NMapBump, float *VerticesBS, float *BlendshapeCoeff, 
                                               float *VMap, float *NMap, unsigned char *RGBMap, float *Pose, 
                                               float *Pose_D2RGB, float *calib_depth, float *calib_rgb, int n_bump, 
                                               int m_bump, int n_rgbd, int m_rgbd""",
                                               KernelsOpenCL.Kernel_Bump,
                                               "Bump_Mapping")
        
        self.Impaint = ElementwiseKernel(self.GPUManager.context, 
                                               "float *Bump, float *RGBMapBump, float *BumpSwap, float *RGBMapBumpSwap, unsigned char *Label, int nbLines, int nbColumns",
                                               KernelsOpenCL.Kernel_Impaint,
                                               "Impaint")
        
        self.vmap_to_nmap = ElementwiseKernel(self.GPUManager.context, 
                                               "float *vmap, float *nmap, int nbLines, int nbColumns",
                                               KernelsOpenCL.Kernel_NMap,
                                               "vmap_to_nmap")
        
        self.DataProc = ElementwiseKernel(self.GPUManager.context, 
                                               "float *Bump, float *WeightMap, float *Vertices, float *Normales, int *Triangles, float *VerticesBS, int nbLines, int nbColumns",
                                               KernelsOpenCL.Kernel_DataProc,
                                               "DataProc")
        
        self.draw_vmap = ElementwiseKernel(self.GPUManager.context, 
                                               "float *vmap, float *nmap, float *color, unsigned int *res, float *Pose, float *Intrinsic, int color_flag, int nbLines, int nbColumns",
                                               KernelsOpenCL.Kernel_Draw,
                                               "draw_vmap")
        
        self.vmap = ElementwiseKernel(self.GPUManager.context, 
                                               "float *VMapBump, float *NMapBump, float *VerticesBS, float *BlendshapeCoeff, int n_bump, int m_bump",
                                               KernelsOpenCL.Kernel_VMap,
                                               "vmap")
        
        self.vmap_E = ElementwiseKernel(self.GPUManager.context, 
                                               "float *VMapBump, float *NMapBump, float *VerticesBS, float *BlendshapeCoeff, float *Bump, float *Pose, int n_bump, int m_bump",
                                               KernelsOpenCL.Kernel_VMap_E,
                                               "vmap_E")
        
        self.impaint_draw = ElementwiseKernel(self.GPUManager.context, 
                                               "unsigned int *Draw, unsigned int *Buffer, float sigma_d, int nbLines, int nbColumns",
                                               KernelsOpenCL.Kernel_ImpaintDraw,
                                               "impaint_draw")
        
        self.segment_occlusion = ElementwiseKernel(self.GPUManager.context, 
                                               "float *vmap, float *nmap, float *Bump, int *Mask, int *occluded, float *depth, float *Pose, float *Pose_D2RGB, float *Intrinsic, float *Intrinsic_rgb, int nbLines, int nbColumns",
                                               KernelsOpenCL.Kernel_Occlusion,
                                               "segment_occlusion")
        
        self.fill = ElementwiseKernel(self.GPUManager.context, 
                                               "float *vmap, float *nmap, float *Bump, float *rgb, int *Mask, int *occluded, float *depth, unsigned char *color_in, float *Pose, float *Pose_D2RGB, float *Intrinsic, float *Intrinsic_rgb, int nbLines, int nbColumns",
                                               KernelsOpenCL.Kernel_Fill,
                                               "fill")
        
        self.bbox = ElementwiseKernel(self.GPUManager.context, 
                                               "float *vmap, int *BBOX, float *Pose, float *Pose_D2RGB, float *Intrinsic, float *Intrinsic_rgb",
                                               KernelsOpenCL.Kernel_MINMAX,
                                               "bbox")
        
        # Allocate on CPU 
        self.draw_result = np.zeros(self.SizeDraw, dtype = np.uint32)
        
        self.BumpImage_d = cl.array.to_device(self.GPUManager.queue, self.Bump)
        self.WeightMap_d = cl.array.to_device(self.GPUManager.queue, self.WeightMap)
        self.labels_d = cl.array.to_device(self.GPUManager.queue, img4)
        
        self.VMapBump_d = cl.array.zeros(self.GPUManager.queue, self.Size, np.float32)
        self.NMapBump_d = cl.array.zeros(self.GPUManager.queue, self.Size, np.float32)
        self.RGBBump_d = cl.array.zeros(self.GPUManager.queue, self.Size, np.float32)
        self.RGBSwap_d = cl.array.zeros(self.GPUManager.queue, self.Size, np.float32)
        self.BumpSwap_d = cl.array.zeros(self.GPUManager.queue, self.Size, np.float32)
        self.VerticesBS_d = cl.array.zeros(self.GPUManager.queue, (28*self.Size[0], self.Size[1], 6), np.float32)
        self.occluded_d = cl.array.zeros(self.GPUManager.queue, 300, np.int32)
        
        self.intrinsic_d = cl.array.zeros(self.GPUManager.queue, 4, np.float32)
        self.intrinsic_RGB_d = cl.array.zeros(self.GPUManager.queue, 4, np.float32)
        self.Pose_d = cl.array.zeros(self.GPUManager.queue, (4,4), np.float32)
        self.Pose_D2RGB_d = cl.array.zeros(self.GPUManager.queue, (4,4), np.float32)
        self.draw_d = cl.array.zeros(self.GPUManager.queue, self.SizeDraw, dtype = np.uint32)
        self.draw_buff_d = cl.array.zeros(self.GPUManager.queue, self.SizeDraw, dtype = np.uint32)
        self.BlendshapeCoeff_d = cl.array.zeros(self.GPUManager.queue, 28, np.float32)
               
        self.Pose_D2RGB_d.set(D2RGB.astype(np.float32))
        
        
    def ComputeWeightAndLabels(self, i_low,i_up,j_low,j_up):
        
        ''' Compute labels for each pixel '''
        Label_img = np.zeros((800,800), dtype = np.uint16)
        Weight_img = np.zeros((800,800, 3), dtype = np.uint16)
        
        for i in range(i_low,i_up):
            for j in range(j_low,j_up):
                print (i,j)
                #P =((125.0 + 25.0*float(i)/100.0)/240.0, (160.0+20.0*float(j)/100.0)/240.0)
                P = (float(i)/800.0, float(j)/800.0)
                if (P[0] > 0):
                    P_cyl = (sqrt(P[0]**2 + P[1]**2), np.arctan(P[1]/P[0]))
                else:
                    P_cyl = (P[1], 0.0)
                idx = 1
                for face in self.BlendShapes[0].Faces:
                    A = self.BlendShapes[0].UV[face[1],:]
                    A_cyl = (sqrt(A[0]**2 + A[1]**2), np.arctan(A[1]/A[0]))
                    B = self.BlendShapes[0].UV[face[3],:]
                    B_cyl = (sqrt(B[0]**2 + B[1]**2), np.arctan(B[1]/B[0]))
                    C = self.BlendShapes[0].UV[face[5],:]
                    C_cyl = (sqrt(C[0]**2 + C[1]**2), np.arctan(C[1]/C[0]))
                    
                    if (LA.norm(A-B) == 0.0 or LA.norm(A-C) == 0.0 or LA.norm(B-C) == 0.0):
                        idx += 1
                        continue
                    
                    '''AB = B-A
                    AC = C-A
                    AP = P-A
                    u = (AP[1]-(AP[0]/AC[0])*AC[1])/(AB[1] - (AB[0]/AC[0])*AC[1])
                    v = (AP[0] - u*AB[0])/AC[0]'''
                    
                    Mat = np.array([[A_cyl[0], B_cyl[0], C_cyl[0]],[A_cyl[1], B_cyl[1], C_cyl[1]],[1.0, 1.0, 1.0]])
                    b = np.array([P_cyl[0], P_cyl[1], 1.0])
                    w = np.dot(LA.inv(Mat),b)
                    if (w[0]>=0 and w[1] >= 0 and w[2]>=0):
                        print w
                        Label_img[i,j] = idx
                        Weight_img[i,j,0] = int(round(w[0]*65535.0))
                        Weight_img[i,j,1] = int(round(w[1]*65535.0))
                        Weight_img[i,j,2] = int(round(w[2]*65535.0))
                        break
                        
                    idx += 1
                    
                    
        cv2.imwrite("Label_img"+str(i_low)+str(i_up)+str(j_low)+str(j_up)+".png", Label_img)
        cv2.imwrite("Weight_img"+str(i_low)+str(i_up)+str(j_low)+str(j_up)+".png", Weight_img)
        
    def LoadBS(self):
        
        currMesh = MyMesh(self.path + "Neutralm.obj")
        currMesh.LoadMesh()
        self.BlendShapes.append(currMesh)
        print "nb points", len(currMesh.Vertices)
        
        
        '''threads = []
        for i in range(8):
            for j in range(8):
                t = Process(target=self.ComputeWeightAndLabels, args = (100*i,100*(i+1),100*j,100*(j+1), ))
                threads.append(t)
                t.start()
            
        for t in threads:
            t.join()'''
        
        self.InnerFaces = []
        self.InnerVertices = []
        self.FixedVertices = []
        
        faceTmp = self.BlendShapes[0].Faces.copy()
        for face in faceTmp:
                
            IsInner = False
            for k in range(3):
                vtx = self.BlendShapes[0].Vertices[face[2*k]]
                if (self.Bump[int(round(vtx[4]*self.Size[0])), int(round(vtx[5]*self.Size[1])), 1] == -1.0):
                    try:
                        self.InnerVertices.remove(vtx[3])
                        self.InnerVertices.append(vtx[3])
                    except:
                        self.InnerVertices.append(vtx[3])
                    if (not IsInner):
                        self.InnerFaces.append(face)
                        IsInner = True
            if (IsInner):
                for k in range(3):
                    vtx = self.BlendShapes[0].Vertices[face[2*k]]
                    if (self.Bump[int(round(vtx[4]*self.Size[0])), int(round(vtx[5]*self.Size[1])), 2] < 0):
                        try:
                            self.InnerVertices.remove(vtx[3])
                            self.InnerVertices.append(vtx[3])
                        except:
                            self.InnerVertices.append(vtx[3])
                    elif (self.Bump[int(round(vtx[4]*self.Size[0])), int(round(vtx[5]*self.Size[1])), 1] == 0.0):
                        try:
                            self.FixedVertices.remove(vtx[3])
                            self.FixedVertices.append(vtx[3])
                        except:
                            self.FixedVertices.append(vtx[3])
                            
        for face in self.InnerFaces:
            for k in range(3):
                idx = 0
                found = False
                for vtx in self.FixedVertices:
                    if (face[2*k] == vtx):
                        face[2*k] = idx
                        face[2*k+1] = vtx
                        found = True
                        break
                    idx += 1
                    
                if (not found):
                    for vtx in self.InnerVertices:
                        if (face[2*k] == vtx):
                            face[2*k] = idx
                            face[2*k+1] = vtx
                            break
                        idx += 1
                        
                        
        #np.savez_compressed("../data/InnerData", FixedVertices = self.FixedVertices, InnerVertices = self.InnerVertices, InnerFaces = self.InnerFaces)
        
        #print self.FixedVertices'''
        
        '''data = np.load("../data/InnerData.npz")
        self.FixedVertices = data["FixedVertices"]
        self.InnerVertices = data["InnerVertices"]
        self.InnerFaces = data["InnerFaces"]'''
        
        '''idx = 0
        for vtx in self.FixedVertices:
            if (self.Bump[int(round(self.BlendShapes[0].Vertices[int(vtx),4]*self.Size[0])), int(round(self.BlendShapes[0].Vertices[int(vtx),5]*self.Size[1])),1] == -1.0):
                self.FixedVertices = np.delete(self.FixedVertices, idx)
                self.InnerVertices = np.hstack((self.InnerVertices, [vtx]))
                idx += 1
                
        idx = 0        
        for vtx in self.InnerVertices:
            if (self.Bump[int(round(self.BlendShapes[0].Vertices[int(vtx),4]*self.Size[0])), int(round(self.BlendShapes[0].Vertices[int(vtx),5]*self.Size[1])),1] != -1.0):
                self.InnerVertices = np.delete(self.InnerVertices, idx)
                self.FixedVertices = np.hstack((self.FixedVertices, [vtx]))
                idx += 1'''
        
        '''Label_in_img = np.zeros((240,240), dtype = np.uint8)
        
        for i in range(240):
            for j in range(240):
                print i,j
                idx = int(self.Bump[i,j,2])
                if (idx == -1):
                    continue
                face = self.BlendShapes[0].Faces[idx]
                
                for k in range(3):
                    vtx = self.BlendShapes[0].Vertices[face[2*k],:]
                    for idx_in in self.InnerVertices:
                        vtx_in = self.BlendShapes[0].Vertices[int(idx_in),:]
                        if (LA.norm(vtx - vtx_in) == 0.0):
                            Label_in_img[i,j] = 255
                            break
        
        cv2.imwrite("Labels_in.png", Label_in_img)'''
        
        '''
        self.BridgeList = []
        
        for face in self.BlendShapes[0].Faces:
            uv0 = self.BlendShapes[0].UV[face[1],:]
            uv1 = self.BlendShapes[0].UV[face[3],:]
            uv2 = self.BlendShapes[0].UV[face[5],:]
            if ((uv0[1]*self.Size[1] < 50 and uv1[1]*self.Size[1] > 100) or
                (uv0[1]*self.Size[1] < 100 and uv1[1]*self.Size[1] > 150)):
                self.BridgeList.append(((round(uv0[0]*self.Size[0]), round(uv0[1]*self.Size[1])),
                                       (round(uv1[0]*self.Size[0]), round(uv1[1]*self.Size[1]))))
            
            if ((uv0[1]*self.Size[1] < 50 and uv2[1]*self.Size[1] > 100) or
                (uv0[1]*self.Size[1] < 100 and uv2[1]*self.Size[1] > 150)):
                self.BridgeList.append(((round(uv0[0]*self.Size[0]), round(uv0[1]*self.Size[1])),
                                       (round(uv2[0]*self.Size[0]), round(uv2[1]*self.Size[1]))))
                
                
            if ((uv1[1]*self.Size[1] < 50 and uv0[1]*self.Size[1] > 100) or
                (uv1[1]*self.Size[1] < 100 and uv0[1]*self.Size[1] > 150)):
                self.BridgeList.append(((round(uv1[0]*self.Size[0]), round(uv1[1]*self.Size[1])),
                                       (round(uv0[0]*self.Size[0]), round(uv0[1]*self.Size[1]))))
                
            if ((uv1[1]*self.Size[1] < 50 and uv2[1]*self.Size[1] > 100) or 
                (uv1[1]*self.Size[1] < 100 and uv2[1]*self.Size[1] > 150)):
                self.BridgeList.append(((round(uv1[0]*self.Size[0]), round(uv1[1]*self.Size[1])),
                                       (round(uv2[0]*self.Size[0]), round(uv2[1]*self.Size[1]))))
                
            if ((uv2[1]*self.Size[1] < 50 and uv0[1]*self.Size[1] > 100) or
                (uv2[1]*self.Size[1] < 100 and uv0[1]*self.Size[1] > 150)):
                self.BridgeList.append(((round(uv2[0]*self.Size[0]), round(uv2[1]*self.Size[1])),
                                       (round(uv0[0]*self.Size[0]), round(uv0[1]*self.Size[1]))))
                
            if ((uv2[1]*self.Size[1] < 50 and uv1[1]*self.Size[1] > 100) or
                (uv2[1]*self.Size[1] < 100 and uv1[1]*self.Size[1] > 150)):
                self.BridgeList.append(((round(uv2[0]*self.Size[0]), round(uv2[1]*self.Size[1])),
                                       (round(uv1[0]*self.Size[0]), round(uv1[1]*self.Size[1]))))
        
        #print self.BridgeList'''
        
        
        for i in range(48):
            if ((i > 1 and i < 14) or i == 17 or i == 18 or i == 19 or i == 22 or i == 26 or i == 27 or i == 34 or i == 35 or i == 42):
                continue
            if (i == 0 or i == 1):
                currMesh = MyMesh(self.path + "0" + str(i) + "m.obj")
            else:
                currMesh = MyMesh(self.path + str(i) + "m.obj")
            currMesh.LoadMesh()
            self.BlendShapes.append(currMesh)
            
    ''' Rescale all blendshapes to match user's landmarks '''
    def Rescale(self, rgbd):
        rgb2depth = rgbd.RGB2Depth.get()
        vmap = rgbd.vmap_d.get()
        
        ''' Compute average factor in X length from outer corner of eyes ''' 
        idx = rgb2depth[rgbd.shape[19][1], rgbd.shape[19][0]]
        if (idx == 0):
            print "No landmark for the left eye"
            return False
        
        left_eye = vmap[idx/rgbd.Size[1], idx%rgbd.Size[1]]
        
        
        idx = rgb2depth[rgbd.shape[28][1], rgbd.shape[28][0]]
        if (idx == 0):
            print "No landmark for the right eye"
            return False
        
        right_eye = vmap[idx/rgbd.Size[1], idx%rgbd.Size[1]]

        eye_v = left_eye - right_eye
        eye_dist = sqrt(eye_v[0]*eye_v[0] + eye_v[1]*eye_v[1] + eye_v[2]*eye_v[2]) #LA.norm(np.array(left_eye - right_eye))
        eye_v = self.BlendShapes[0].Vertices[FACIAL_LANDMARKS[19],0:3] - self.BlendShapes[0].Vertices[FACIAL_LANDMARKS[28],0:3]
        eye_dist_mesh = sqrt(eye_v[0]*eye_v[0] + eye_v[1]*eye_v[1] + eye_v[2]*eye_v[2])  #LA.norm(np.array(self.BlendShapes[0].Vertices[FACIAL_LANDMARKS[19],0:3] -
            #                             self.BlendShapes[0].Vertices[FACIAL_LANDMARKS[28],0:3]))
        if (eye_dist_mesh == 0.0):
            print self.BlendShapes[0].Vertices[FACIAL_LANDMARKS[19],::]
            print self.BlendShapes[0].Vertices[FACIAL_LANDMARKS[19],::]
            eye_dist_mesh = 1.0
        fact = eye_dist/eye_dist_mesh
        
        ''' Compute average factor in X length from inner corner of eyes '''
        idx = rgb2depth[rgbd.shape[22][1], rgbd.shape[22][0]]
        if (idx == 0):
            print "No landmark for the inner left eye"
            return False
        
        left_eye = vmap[idx/rgbd.Size[1], idx%rgbd.Size[1]]
        
        
        idx = rgb2depth[rgbd.shape[25][1], rgbd.shape[25][0]]
        if (idx == 0):
            print "No landmark for the inner right eye"
            return False
        
        right_eye = vmap[idx/rgbd.Size[1], idx%rgbd.Size[1]]

        eye_v = left_eye - right_eye
        eye_dist = sqrt(eye_v[0]*eye_v[0] + eye_v[1]*eye_v[1] + eye_v[2]*eye_v[2])
        eye_v = self.BlendShapes[0].Vertices[FACIAL_LANDMARKS[22],0:3] - self.BlendShapes[0].Vertices[FACIAL_LANDMARKS[25],0:3]
        eye_dist_mesh = sqrt(eye_v[0]*eye_v[0] + eye_v[1]*eye_v[1] + eye_v[2]*eye_v[2]) #LA.norm(np.array(self.BlendShapes[0].Vertices[FACIAL_LANDMARKS[22],0:3] -
            #                             self.BlendShapes[0].Vertices[FACIAL_LANDMARKS[25],0:3]))
        if (eye_dist_mesh == 0.0):
            eye_dist_mesh = 1.0
        fact = fact + eye_dist/eye_dist_mesh
       
        ''' Compute average factor in X length from mouth '''
        idx = rgb2depth[rgbd.shape[31][1], rgbd.shape[31][0]]
        if (idx == 0):
            print "No landmark for the lrft mouth"
            return False
        
        left_eye = vmap[idx/rgbd.Size[1], idx%rgbd.Size[1]]
        
        
        idx = rgb2depth[rgbd.shape[37][1], rgbd.shape[37][0]]
        if (idx == 0):
            print "No landmark for the right mouth"
            return False
        
        right_eye = vmap[idx/rgbd.Size[1], idx%rgbd.Size[1]]

        eye_v = left_eye - right_eye
        eye_dist = sqrt(eye_v[0]*eye_v[0] + eye_v[1]*eye_v[1] + eye_v[2]*eye_v[2])
        eye_v = self.BlendShapes[0].Vertices[FACIAL_LANDMARKS[31],0:3] - self.BlendShapes[0].Vertices[FACIAL_LANDMARKS[37],0:3]
        eye_dist_mesh = sqrt(eye_v[0]*eye_v[0] + eye_v[1]*eye_v[1] + eye_v[2]*eye_v[2])
        #eye_dist_mesh = LA.norm(np.array(self.BlendShapes[0].Vertices[FACIAL_LANDMARKS[31],0:3] -
        #                                 self.BlendShapes[0].Vertices[FACIAL_LANDMARKS[37],0:3]))
        fact = fact + eye_dist/eye_dist_mesh
       
        fact_x = fact/3.0
        
        ''' Compute average factor in Y length from nose '''
        idx = rgb2depth[rgbd.shape[10][1], rgbd.shape[10][0]]
        if (idx == 0):
            print "No landmark for the up nose"
            return False
        
        left_eye = vmap[idx/rgbd.Size[1], idx%rgbd.Size[1]]
        
        
        idx = rgb2depth[rgbd.shape[16][1], rgbd.shape[16][0]]
        if (idx == 0):
            print "No landmark for the down nose"
            return False
        
        right_eye = vmap[idx/rgbd.Size[1], idx%rgbd.Size[1]]


        eye_v = left_eye - right_eye
        eye_dist = sqrt(eye_v[0]*eye_v[0] + eye_v[1]*eye_v[1] + eye_v[2]*eye_v[2])
        eye_v = self.BlendShapes[0].Vertices[FACIAL_LANDMARKS[10],0:3] - self.BlendShapes[0].Vertices[FACIAL_LANDMARKS[16],0:3]
        eye_dist_mesh = sqrt(eye_v[0]*eye_v[0] + eye_v[1]*eye_v[1] + eye_v[2]*eye_v[2])
        
        '''eye_dist = LA.norm(np.array(left_eye - right_eye))
        eye_dist_mesh = LA.norm(np.array(self.BlendShapes[0].Vertices[FACIAL_LANDMARKS[10],0:3] - 
                                         self.BlendShapes[0].Vertices[FACIAL_LANDMARKS[16],0:3]))     '''
        fact_y = eye_dist/eye_dist_mesh
        
        #fact = fact/4.
        print "Scale factor: ", fact_x, fact_y
        
        for mesh in self.BlendShapes:
            mesh.Vertices[:,0] = mesh.Vertices[:,0]*fact_x
            mesh.Vertices[:,1] = mesh.Vertices[:,1]*fact_y
            mesh.Vertices[:,2] = mesh.Vertices[:,2]*fact_x
            
        return True
    
    def RefineAlignment(self, rgbd):
        rgb2depth = rgbd.RGB2Depth.get()
        vmap = rgbd.vmap_d.get()
        res = np.identity(4)
        delta_transfo = np.identity(4)
        
        Jac = np.zeros((3*51,6), dtype = np.float32)
        Jac_B = np.zeros((3*51,1), dtype = np.float32)
        
        landmark_mesh = np.ones(4, dtype = np.float32)
#        landmark_mesh_nmle = np.ones(3, dtype = np.float32)
        
        max_iter = 20
        for i in range(max_iter):
            b = np.zeros(6, np.float32)
            A = np.zeros((6,6), np.float32)
            
            # Build the jacobian matrix
            for k in range(51):
                idx = rgb2depth[rgbd.shape[k][1], rgbd.shape[k][0]]
                if (idx == 0):
                    print "No landmark"
                    continue#return False
                landmark = vmap[idx/rgbd.Size[1], idx%rgbd.Size[1]]
                landmark_mesh = np.ones(4, dtype = np.float32)
                landmark_mesh[0:3] = self.BlendShapes[0].Vertices[FACIAL_LANDMARKS[k],0:3]
                landmark_mesh = np.dot(res, landmark_mesh)
                
                Jac[3*k][:] = [1.0, 0.0, 0.0, 0.0, 2.0*landmark_mesh[2], -2.0*landmark_mesh[1]]
                Jac[3*k+1][:] = [0.0, 1.0, 0.0, -2.0*landmark_mesh[2], 0.0, 2.0*landmark_mesh[0]]
                Jac[3*k+2][:] = [0.0, 0.0, 1.0, 2.0*landmark_mesh[1], -2.0*landmark_mesh[0], 0.0]
                
                Jac_B[3*k][0] = landmark[0] - landmark_mesh[0]
                Jac_B[3*k+1][0] = landmark[1] - landmark_mesh[1]
                Jac_B[3*k+2][0] = landmark[2] - landmark_mesh[2]
                
#                landmark_mesh_nmle[0:3] = self.BlendShapes[0].Normales[FACIAL_LANDMARKS[k],0:3]
#                landmark_mesh_nmle = np.dot(res[0:3,0:3], landmark_mesh_nmle)
                
#                Jac[k][:] = [landmark_mesh_nmle[0], landmark_mesh_nmle[1], landmark_mesh_nmle[2],
#                    -landmark[2]*landmark_mesh_nmle[1] + landmark[1]*landmark_mesh_nmle[2],
#                    landmark[2]*landmark_mesh_nmle[0] - landmark[0]*landmark_mesh_nmle[2],
#                    -landmark[1]*landmark_mesh_nmle[0] + landmark[0]*landmark_mesh_nmle[1]]
#                
#                Jac_B[k][0] = landmark_mesh_nmle[0]*(landmark[0] - landmark_mesh[0]) + landmark_mesh_nmle[1]*(landmark[1] - landmark_mesh[1]) + landmark_mesh_nmle[2]*(landmark[2] - landmark_mesh[2])
#                
            
            A = np.dot(Jac.transpose(), Jac)
            b = np.dot(Jac.transpose(), Jac_B).reshape(6)
            
            det = LA.det(A)
            if (det < 1.0e-10):
                print "determinant null"
                return False
       
            #delta_qsi = -LA.tensorsolve(A, b)
            #delta_transfo = LA.inv(Exponential(delta_qsi))
            
            delta_qsi = LA.tensorsolve(A, b)
            norm = (delta_qsi[3]*delta_qsi[3] + delta_qsi[4]*delta_qsi[4] + delta_qsi[5]*delta_qsi[5])
            if (norm > 1.0) :
				delta_qsi = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
			
            quat = [sqrt(1.0 - norm), delta_qsi[3], delta_qsi[4], delta_qsi[5]]

            delta_transfo[0:3,0:3] = quaternion2matrix(quat);
            delta_transfo[0:3,3] = delta_qsi[0:3]
            
            res = np.dot(delta_transfo, res)
        
        for mesh in self.BlendShapes:
            mesh.Normales[:,0:3] = np.dot(mesh.Normales[:,0:3], res[0:3,0:3].T)
            mesh.Vertices[:,0:3] = np.dot(mesh.Vertices[:,0:3], res[0:3,0:3].T)
            mesh.Vertices[:,0:3] = mesh.Vertices[:,0:3] + res[0:3,3]
            
        print res
        return True
    
    def AlignToFace(self, rgbd):
        rgb2depth = rgbd.RGB2Depth.get()
        vmap = rgbd.vmap_d.get()
        
        idx = rgb2depth[rgbd.shape[13][1], rgbd.shape[13][0]]
        if (idx == 0):
            print "No landmark for the nose"
            return False
        
        nose = np.array(vmap[idx/rgbd.Size[1], idx%rgbd.Size[1]])
        nose_mesh = np.array(self.BlendShapes[0].Vertices[FACIAL_LANDMARKS[13],0:3])
        
        idx = rgb2depth[rgbd.shape[19][1], rgbd.shape[19][0]]
        if (idx == 0):
            print "No landmark for the left eye"
            return False
        
        left_eye = np.array(vmap[idx/rgbd.Size[1], idx%rgbd.Size[1]])
        left_eye_mesh = np.array(self.BlendShapes[0].Vertices[FACIAL_LANDMARKS[19],0:3])
        
        
        idx = rgb2depth[rgbd.shape[28][1], rgbd.shape[28][0]]
        if (idx == 0):
            print "No landmark for the right eye"
            return False
        
        right_eye = np.array(vmap[idx/rgbd.Size[1], idx%rgbd.Size[1]])
        right_eye_mesh = np.array(self.BlendShapes[0].Vertices[FACIAL_LANDMARKS[28],0:3])
        
        v1 = np.cross(left_eye - nose, right_eye - nose)
        v1 = v1/sqrt(v1[0]*v1[0] + v1[1]*v1[1] + v1[2]*v1[2]) #LA.norm(v1)
        
        v2 = np.cross(left_eye_mesh - nose_mesh, right_eye_mesh - nose_mesh)
        v2 = v2/sqrt(v2[0]*v2[0] + v2[1]*v2[1] + v2[2]*v2[2]) #LA.norm(v2)
        
        rotation_axis = np.cross(v1,v2)
        
        rotation_axis = rotation_axis/sqrt(rotation_axis[0]*rotation_axis[0] + rotation_axis[1]*rotation_axis[1] + rotation_axis[2]*rotation_axis[2]) #LA.norm(rotation_axis)
        rotation_angle = -acos(np.dot(v1,v2))
        q = [cos(rotation_angle/2.), rotation_axis[0]*sin(rotation_angle/2.), rotation_axis[1]*sin(rotation_angle/2.), rotation_axis[2]*sin(rotation_angle/2.)]
        
        norm_q = q[0]*q[0] + q[1]*q[1] + q[2]*q[2] + q[3]*q[3] #LA.norm(q)
        s = 1./(norm_q) #LA.norm(q)**2
        
        Rotation = np.array([[1.-2.*s*(q[2]**2 + q[3]**2), 2.*s*(q[1]*q[2] - q[3]*q[0]), 2.*s*(q[1]*q[3] + q[2]*q[0])],
                     [2.*s*(q[1]*q[2] + q[3]*q[0]), 1.-2.*s*(q[1]**2 + q[3]**2), 2.*s*(q[2]*q[3] - q[1]*q[0])],
                     [2.*s*(q[1]*q[3]-q[2]*q[0]), 2.*s*(q[2]*q[3] + q[1]*q[0]), 1.-2.*s*(q[1]**2 + q[2]**2)]])
    
        
        for mesh in self.BlendShapes:
            mesh.Normales[:,0:3] = np.dot(mesh.Normales[:,0:3], Rotation.T)
            mesh.Vertices[:,0:3] = np.dot(mesh.Vertices[:,0:3], Rotation.T)
            
        
        idx = rgb2depth[rgbd.shape[13][1], rgbd.shape[13][0]]
        nose = vmap[idx/rgbd.Size[1], idx%rgbd.Size[1]]
        nose_mesh = self.BlendShapes[0].Vertices[FACIAL_LANDMARKS[13],0:3]
        
        translation = nose-nose_mesh
        
        for mesh in self.BlendShapes:
            mesh.Vertices[:,0] = mesh.Vertices[:,0] + translation[0]
            mesh.Vertices[:,1] = mesh.Vertices[:,1] + translation[1]
            mesh.Vertices[:,2] = mesh.Vertices[:,2] + translation[2]
        
        return self.RefineAlignment(rgbd)
        
    
    def DrawLandmarks(self, rgbd, Pose, intrinsic, canvas, idx_L):
        
        rgb2depth = rgbd.RGB2Depth.get()
        vmap = rgbd.vmap_d.get()
        
        landmark = np.ones(4, dtype = np.float32)
        landmark_mesh = np.ones(4, dtype = np.float32)
        pix = np.array([0., 0., 1.])
        
        for k in range(51):        #range(idx_L,idx_L+1):
            idx = rgb2depth[rgbd.shape[k][1], rgbd.shape[k][0]]
            if (idx == 0):
                #print "No landmark"
                continue
            landmark[0:3] = vmap[idx/rgbd.Size[1], idx%rgbd.Size[1],:]
            #landmark = np.dot(Pose, landmark)  
            landmark_mesh[0:3] = self.BlendShapes[0].Vertices[FACIAL_LANDMARKS[k],0:3]
            
            for bs_idx in range(1,28):
                landmark_mesh[0:3] = landmark_mesh[0:3] + (self.BlendShapes[bs_idx].Vertices[FACIAL_LANDMARKS[k],0:3]-self.BlendShapes[0].Vertices[FACIAL_LANDMARKS[k],0:3])*self.BlendshapeCoeff[bs_idx]
            
            landmark_mesh = np.dot(Pose, landmark_mesh)
            
            pix[0] = landmark[0]/landmark[2]
            pix[1] = landmark[1]/landmark[2]
            pix = np.dot(intrinsic, pix)
            column_index = int(round(pix[0]))
            line_index = int(round(pix[1]))
            
            poly = [(column_index-2, line_index-2), (column_index-2, line_index+2), (column_index+2, line_index+2), (column_index+2, line_index+2)]
            canvas.create_polygon(*poly, fill='red')
                
            #print k, landmark_mesh
            pix[0] = landmark_mesh[0]/landmark_mesh[2]
            pix[1] = landmark_mesh[1]/landmark_mesh[2]
            pix = np.dot(intrinsic, pix)
            column_index = int(round(pix[0]))
            line_index = int(round(pix[1]))
            
            poly = [(column_index-2, line_index-2), (column_index-2, line_index+2), (column_index+2, line_index+2), (column_index+2, line_index+2)]
            canvas.create_polygon(*poly, fill='green')
        
    def ChangeLandmark(self, idx, inc):
        FACIAL_LANDMARKS[idx] = FACIAL_LANDMARKS[idx] + inc
        print FACIAL_LANDMARKS[idx]
        
    def LoadAffineTransfo(self):
        data = np.load("AffineTransfo.npz")
        self.MatList1 = data["MatList1"]
        self.MatList2 = data["MatList2"]
        
        
    def ComputeAffineTransfo(self):
        '''Inititialisation'''
        RefMesh = self.BlendShapes[0]
        
        self.TransfoExpression = np.zeros((RefMesh.Faces.shape[0], len(self.BlendShapes)-1, 3, 3))
            
        print RefMesh.Faces.shape[0]
        
        So = np.zeros((3,3))
        Si = np.zeros((3,3))
        idxFace = 0
        for face in RefMesh.Faces:
            # Compute normal and weight of the face
            nmle = RefMesh.GetWeightedNormal(face)
            
            # Compute tetrahedron for b0
            summit4 = (RefMesh.Vertices[face[0]] + RefMesh.Vertices[face[2]]+ RefMesh.Vertices[face[4]])[0:3]/3. + nmle
            
            So[0:3,0] = RefMesh.Vertices[face[2]][0:3] - RefMesh.Vertices[face[0]][0:3]
            So[0:3,1] = RefMesh.Vertices[face[4]][0:3] - RefMesh.Vertices[face[0]][0:3]
            So[0:3,2] = summit4 - RefMesh.Vertices[face[0]][0:3]
            
            # Go through all other blendshapes
            idxMesh = 0
            for mesh in self.BlendShapes[1::]:
                # Compute normal and weight of the face
                nmlei = mesh.GetWeightedNormal(face)
            
                # Compute tetrahedron for b0
                summit4 = (mesh.Vertices[face[0]] + mesh.Vertices[face[2]]+ mesh.Vertices[face[4]])[0:3]/3. + nmlei
                
                Si[0:3,0] = mesh.Vertices[face[2]][0:3] - mesh.Vertices[face[0]][0:3]
                Si[0:3,1] = mesh.Vertices[face[4]][0:3] - mesh.Vertices[face[0]][0:3]
                Si[0:3,2] = summit4 - mesh.Vertices[face[0]][0:3]
                
                self.TransfoExpression[idxFace,idxMesh] = np.dot(Si, LA.inv(So))
                
                idxMesh += 1
                
            idxFace += 1
        
        print "TransfoExpression computed"
            
        # Compute Matrix F that fix points on the backface.
        F = sp.lil_matrix((3*RefMesh.Vertices.shape[0], 3*RefMesh.Vertices.shape[0]))
        for ind in range(RefMesh.Vertices.shape[0]):
            if (RefMesh.IsBack[ind]):
                F[3*ind:3*ind + 3, 3*ind:3*ind + 3] = np.identity(3)
         
        # Compute Matrix G that transform vertices to edges
        G = sp.lil_matrix((6*RefMesh.Faces.shape[0], 3*RefMesh.Vertices.shape[0]))
        indx = 0
        for face in RefMesh.Faces:
            G[6 * indx:6 * indx+3, 3*face[0]:3*face[0]+3] = -np.identity(3)
            G[6 * indx+3:6 * indx+6, 3*face[0]:3*face[0]+3] = -np.identity(3)
            
            G[6 * indx:6 * indx+3, 3*face[2]:3*face[2]+3] = np.identity(3)
            G[6 * indx+3:6 * indx+6, 3*face[4]:3*face[4]+3] = np.identity(3)
            
            indx += 1
            
        print "Matrix G computed"
        
        nu = 100.0
        self.MatList1 = []
        self.MatList2 = []
        for indx_mesh in range(len(self.BlendShapes)-1):
            print indx_mesh
            # Compute the affine transfo matrix
            H = sp.lil_matrix((6*RefMesh.Faces.shape[0], 6*RefMesh.Faces.shape[0]))
            indx = 0
            for face in RefMesh.Faces:
                H[6 * indx:6 * indx+3, 6 * indx:6 * indx+3] = self.TransfoExpression[indx, indx_mesh]
                H[6 * indx+3:6 * indx+6, 6 * indx+3:6 * indx+6] = self.TransfoExpression[indx, indx_mesh]
                indx += 1
                
            print "Matrix H computed"
                
            MatTmp = G.T * G + nu*F
            MatT = (G.T * H) * G + nu*F
            self.MatList1.append(MatTmp)
            self.MatList2.append(MatT)
            np.savez_compressed("../data/AffineTransfo", MatList1 = self.MatList1, MatList2 = self.MatList2)
        
        print "MatList1/2 computed"
        
    def PopulateMatrix(self, indx_low, indx_up, offset):
        idx = offset+indx_low
        for face in self.BlendShapes[0].Faces[indx_low:indx_up]:
            for k in range(3):
                v_idx = face[2*k]
                self.A[3*idx:3*idx+3, 3*v_idx:3*v_idx+3] = -np.identity(3)
                self.A[3*idx:3*idx+3, 3*face[2*((k+1)%3)]:3*face[2*((k+1)%3)]+3] = np.identity(3)
                pt = self.BlendShapes[0].T_Vertices[face[2*((k+1)%3)]][0:3] - self.BlendShapes[0].T_Vertices[v_idx][0:3]
                #Rotate with the quaternion
                Rpt = np.dot(self.BlendShapes[0].Rotations[v_idx], pt)
                self.b1[3*idx] = Rpt[0]
                self.b1[3*idx+1] = Rpt[1]
                self.b1[3*idx+2] = Rpt[2]
                idx += 1
                
                self.A[3*idx:3*idx+3, 3*v_idx:3*v_idx+3] = -np.identity(3)
                self.A[3*idx:3*idx+3, 3*face[2*((k+2)%3)]:3*face[2*((k+2)%3)]+3] = np.identity(3)
                pt = self.BlendShapes[0].T_Vertices[face[2*((k+2)%3)]][0:3] - self.BlendShapes[0].T_Vertices[v_idx][0:3]
                #Rotate with the quaternion
                Rpt = np.dot(self.BlendShapes[0].Rotations[v_idx], pt)
                self.b1[3*idx] = Rpt[0]
                self.b1[3*idx+1] = Rpt[1]
                self.b1[3*idx+2] = Rpt[2]
                idx += 1
        
    def AffectDefo(self, indx_low, indx_up):
        idx = 0
        for (vtx,nmle) in zip(self.BlendShapes[0].T_Vertices[indx_low:indx_up], self.BlendShapes[0].T_Normales[indx_low, indx_up]):
            vtx[0] = self.xres[3*idx]
            vtx[1] = self.xres[3*idx+1]
            vtx[2] = self.xres[3*idx+2]
            nmle[0] = 0.
            nmle[1] = 0.
            nmle[2] = 0.
            idx += 1
        
        
    def ElasticRegistration(self, rgbd):
        self.BlendShapes[0].T_Vertices = self.BlendShapes[0].Vertices.copy()
        self.BlendShapes[0].T_Normales = self.BlendShapes[0].Normales.copy()
        
        rgb2depth = rgbd.RGB2Depth.get()
        vmap = rgbd.vmap_d.get()
                                      
        nb_col = 3*self.BlendShapes[0].Vertices.shape[0]
        nb_lines = 3*36 + 3*6*self.BlendShapes[0].Faces.shape[0]
            
        self.A = sp.lil_matrix((nb_lines, nb_col), dtype = np.float32)
        self.b1 = np.zeros(nb_lines, dtype = np.float32)
        
        '''/****Compute local tangent plane transforms*****/'''
        self.BlendShapes[0].ComputeTgtPlane()
        
        for iter in range(3):
            '''/****Point correspondences***/
    
        		/****Solve linear system*****/
                Build Matrix A
                Nx -Ny -Nz 0 ......... 0]
                [0 0 0 -Nx -Ny -Nz 0 ... 0]
                ...
                [0............ -Nx -Ny -Nz]
                [0..0...1....-1 .......]
                [0..0...0 1...0 -1 ....]
                [0..0...0 0 1.0 0 -1...]*/'''
            
            '''/*****************************Add landmarks******************************************/'''
            weight = 10.0
            for i in range(36):    #51
                '''if (i > 18 and i < 29):
                    weight = 10.0
                else:
                    weight = 0.1'''
                idx = rgb2depth[rgbd.shape[i][1], rgbd.shape[i][0]]
                if (vmap[idx/rgbd.Size[1], idx%rgbd.Size[1],2] != 0.0):
                    self.A[3*i:3*i+3, 3*FACIAL_LANDMARKS[i]:3*FACIAL_LANDMARKS[i]+3] = weight*np.identity(3)
                    self.b1[3*i:3*i+3] = weight*vmap[idx/rgbd.Size[1], idx%rgbd.Size[1],:]
        
            
            '''/***************Populate matrix from neighboors of the vertices***************************/'''
            '''nb_faces = self.BlendShapes[0].Faces.shape[0]
            step = nb_faces/32
            threads = []
            for i in range(nb_faces/32 + 1):
                    t = Process(target=self.PopulateMatrix, args = (i*step, step*(i+1), 51, ))
                    threads.append(t)
                    t.start()
                
            for t in threads:
                t.join()'''
                
            idx = 36
            for face in self.BlendShapes[0].Faces:
                for k in range(3):
                    v_idx = face[2*k]
                    self.A[3*idx:3*idx+3, 3*v_idx:3*v_idx+3] = -1.0*np.identity(3)
                    self.A[3*idx:3*idx+3, 3*face[2*((k+1)%3)]:3*face[2*((k+1)%3)]+3] = 1.0*np.identity(3)
                    pt = self.BlendShapes[0].T_Vertices[face[2*((k+1)%3)]][0:3] - self.BlendShapes[0].T_Vertices[v_idx][0:3]
                    #Rotate with the quaternion
                    Rpt = np.dot(self.BlendShapes[0].Rotations[v_idx], pt)
                    self.b1[3*idx] = 1.0*Rpt[0]
                    self.b1[3*idx+1] = 1.0*Rpt[1]
                    self.b1[3*idx+2] = 1.0*Rpt[2]
                    idx += 1
                    
                    self.A[3*idx:3*idx+3, 3*v_idx:3*v_idx+3] = -1.0*np.identity(3)
                    self.A[3*idx:3*idx+3, 3*face[2*((k+2)%3)]:3*face[2*((k+2)%3)]+3] = 1.0*np.identity(3)
                    pt = self.BlendShapes[0].T_Vertices[face[2*((k+2)%3)]][0:3] - self.BlendShapes[0].T_Vertices[v_idx][0:3]
                    #Rotate with the quaternion
                    Rpt = np.dot(self.BlendShapes[0].Rotations[v_idx], pt)
                    self.b1[3*idx] = 1.0*Rpt[0]
                    self.b1[3*idx+1] = 1.0*Rpt[1]
                    self.b1[3*idx+2] = 1.0*Rpt[2]
                    idx += 1
            
            #MatA = np.dot(A.T,A)
            #b = np.dot(A.T, b1)
            #xres = LA.tensorsolve(MatA, b)
            self.xres = spsolve(self.A.T*self.A, self.A.T*self.b1)
        
            print self.xres[0:3]
            print self.BlendShapes[0].Vertices[0]
            
            ''' Affect result to mesh '''
            '''nb_vtx = self.BlendShapes[0].T_Vertices.shape[0]
            step = nb_vtx/32
            threads = []
            for i in range(nb_vtx/32 + 1):
                    t = Process(target=self.AffectDefo, args = (i*step, step*(i+1), ))
                    threads.append(t)
                    t.start()
                
            for t in threads:
                t.join()'''
                
            idx = 0
            for (vtx,nmle) in zip(self.BlendShapes[0].T_Vertices, self.BlendShapes[0].T_Normales):
                vtx[0] = self.xres[3*idx]
                vtx[1] = self.xres[3*idx+1]
                vtx[2] = self.xres[3*idx+2]
                nmle[0] = 0.
                nmle[1] = 0.
                nmle[2] = 0.
                idx += 1
                
        '''/***************Compute normals*******************/'''
        
        for face in self.BlendShapes[0].Faces:
            # Compute normal and weight of the face
            v1 = self.BlendShapes[0].T_Vertices[face[2]] - self.BlendShapes[0].T_Vertices[face[0]]
            v2 = self.BlendShapes[0].T_Vertices[face[4]] - self.BlendShapes[0].T_Vertices[face[0]]
            nmle = np.cross(v1[0:3],v2[0:3])
            nmle = nmle / LA.norm(nmle)
            
            self.BlendShapes[0].T_Normales[face[0], 0] = self.BlendShapes[0].T_Normales[face[0], 0] + nmle[0]
            self.BlendShapes[0].T_Normales[face[0], 1] = self.BlendShapes[0].T_Normales[face[0], 1] + nmle[1]
            self.BlendShapes[0].T_Normales[face[0], 2] = self.BlendShapes[0].T_Normales[face[0], 2] + nmle[2]
            
            self.BlendShapes[0].T_Normales[face[2], 0] = self.BlendShapes[0].T_Normales[face[2], 0] + nmle[0]
            self.BlendShapes[0].T_Normales[face[2], 1] = self.BlendShapes[0].T_Normales[face[2], 1] + nmle[1]
            self.BlendShapes[0].T_Normales[face[2], 2] = self.BlendShapes[0].T_Normales[face[2], 2] + nmle[2]
            
            self.BlendShapes[0].T_Normales[face[4], 0] = self.BlendShapes[0].T_Normales[face[4], 0] + nmle[0]
            self.BlendShapes[0].T_Normales[face[4], 1] = self.BlendShapes[0].T_Normales[face[4], 1] + nmle[1]
            self.BlendShapes[0].T_Normales[face[4], 2] = self.BlendShapes[0].T_Normales[face[4], 2] + nmle[2]
            
        for nmle in self.BlendShapes[0].T_Normales:  
            if (LA.norm(nmle) > 0.):
                nmle = nmle / LA.norm(nmle)
            
        self.BlendShapes[0].Vertices = self.BlendShapes[0].T_Vertices.copy()
        self.BlendShapes[0].Normales = self.BlendShapes[0].T_Normales.copy()
        
        '''/***********************Transfer expression deformation******************************/'''
        
        #boV = self.BlendShapes[0].Vertices.copy()
        boV = np.zeros(nb_col, dtype = np.float32)
        idx = 0
        for vtx in self.BlendShapes[0].Vertices:
            boV[idx] = vtx[0]
            boV[idx+1] = vtx[1]
            boV[idx+2] = vtx[2]
            idx += 3
        
        indx_mesh = 0
        for mesh in self.BlendShapes[1::]:
            
            #xres = LA.tensorsolve(self.MatList1[indx_mesh], np.dot(self.MatList2[indx_mesh], boV))
            xres = spsolve(self.MatList1[indx_mesh], self.MatList2[indx_mesh] * boV)
            indx_mesh += 1
            
            ''' Affect result to mesh '''
            idx = 0
            for (vtx,nmle) in zip(mesh.Vertices, mesh.Normales):
                vtx[0] = xres[3*idx]
                vtx[1] = xres[3*idx+1]
                vtx[2] = xres[3*idx+2]
                nmle[0] = 0.
                nmle[1] = 0.
                nmle[2] = 0.
                idx += 1
                
            '''/***************Compute normals*******************/'''
            for face in mesh.Faces:
                # Compute normal and weight of the face
                v1 = mesh.Vertices[face[2]] - mesh.Vertices[face[0]]
                v2 = mesh.Vertices[face[4]] - mesh.Vertices[face[0]]
                nmle = np.cross(v1[0:3],v2[0:3])
                nmle = nmle / LA.norm(nmle)
                
                mesh.Normales[face[0], 0] = mesh.Normales[face[0], 0] + nmle[0]
                mesh.Normales[face[0], 1] = mesh.Normales[face[0], 1] + nmle[1]
                mesh.Normales[face[0], 2] = mesh.Normales[face[0], 2] + nmle[2]
                
                mesh.Normales[face[2], 0] = mesh.Normales[face[2], 0] + nmle[0]
                mesh.Normales[face[2], 1] = mesh.Normales[face[2], 1] + nmle[1]
                mesh.Normales[face[2], 2] = mesh.Normales[face[2], 2] + nmle[2]
                
                mesh.Normales[face[4], 0] = mesh.Normales[face[4], 0] + nmle[0]
                mesh.Normales[face[4], 1] = mesh.Normales[face[4], 1] + nmle[1]
                mesh.Normales[face[4], 2] = mesh.Normales[face[4], 2] + nmle[2]
                
            for nmle in mesh.Normales:  
                if (LA.norm(nmle) > 0.):
                    nmle = nmle / LA.norm(nmle)
        
    
    def InitDefoInner(self):
        '''/****Compute local tangent plane transforms*****/'''
        self.BlendShapes[0].ComputeTgtPlaneInner(self.InnerFaces, self.FixedVertices, self.InnerVertices)
        
        
    def DeformationTransfer(self):
        #self.BlendShapes[0].T_Vertices = self.BlendShapes[0].Vertices.copy()
        #self.BlendShapes[0].T_Normales = self.BlendShapes[0].Normales.copy()
        
        self.BlendShapes[0].Vertices = self.BlendShapes[0].T_Vertices.copy()
        self.BlendShapes[0].Normales = self.BlendShapes[0].T_Normales.copy()
        
        vmap = self.VMapBump_d.get()
                                      
        nb_col = 3*(len(self.FixedVertices) + len(self.InnerVertices))
        nb_lines = 3*len(self.FixedVertices) + 3*6*len(self.InnerFaces)
            
        A = sp.lil_matrix((nb_lines, nb_col), dtype = np.float32)
        b1 = np.zeros(nb_lines, dtype = np.float32)
        
        for iter in range(3):
            '''/****Point correspondences***/
    
        		/****Solve linear system*****/
                Build Matrix A
                Nx -Ny -Nz 0 ......... 0]
                [0 0 0 -Nx -Ny -Nz 0 ... 0]
                ...
                [0............ -Nx -Ny -Nz]
                [0..0...1....-1 .......]
                [0..0...0 1...0 -1 ....]
                [0..0...0 0 1.0 0 -1...]*/'''
            
            '''/*****************************Add Fixed points******************************************/'''
            idx = 0
            for vtx in self.FixedVertices:
                A[3*idx:3*idx+3, 3*idx:3*idx+3] = 0.1*np.identity(3)
                
                v_in = vmap[int(round(self.BlendShapes[0].Vertices[int(vtx),4]*self.Size[0])), int(round(self.BlendShapes[0].Vertices[int(vtx),5]*self.Size[1])),:]

                if (LA.norm(v_in) > 0.0):
                    b1[3*idx:3*idx+3] = 0.1*v_in
                    #print (idx, vmap[int(round(self.BlendShapes[0].Vertices[int(vtx),4]*self.Size[0])), int(round(self.BlendShapes[0].Vertices[int(vtx),5]*self.Size[1])),:])                                
                else:
                    b1[3*idx:3*idx+3] = 0.1*self.BlendShapes[0].Vertices[int(vtx)][0:3]
                idx += 1
            
            '''/***************Populate matrix from neighboors of the vertices***************************/'''
            for face in self.InnerFaces:
                for k in range(3):
                    v_idx = int(face[2*k]) # index in inner vertices/ face[2*k+1] = index in global vertices
                    A[3*idx:3*idx+3, 3*v_idx:3*v_idx+3] = -np.identity(3)
                    A[3*idx:3*idx+3, 3*face[2*((k+1)%3)]:3*face[2*((k+1)%3)]+3] = np.identity(3)
                    pt = self.BlendShapes[0].Vertices[face[2*((k+1)%3)+1]][0:3] - self.BlendShapes[0].Vertices[face[2*k+1]][0:3]
                    #Rotate with the quaternion
                    Rpt = np.dot(self.BlendShapes[0].Rotations[v_idx], pt)
                    b1[3*idx] = Rpt[0]
                    b1[3*idx+1] = Rpt[1]
                    b1[3*idx+2] = Rpt[2]
                    idx += 1
                    
                    A[3*idx:3*idx+3, 3*v_idx:3*v_idx+3] = -np.identity(3)
                    A[3*idx:3*idx+3, 3*face[2*((k+2)%3)]:3*face[2*((k+2)%3)]+3] = np.identity(3)
                    pt = self.BlendShapes[0].Vertices[face[2*((k+2)%3)+1]][0:3] - self.BlendShapes[0].Vertices[face[2*k+1]][0:3]
                    #Rotate with the quaternion
                    Rpt = np.dot(self.BlendShapes[0].Rotations[v_idx], pt)
                    b1[3*idx] = Rpt[0]
                    b1[3*idx+1] = Rpt[1]
                    b1[3*idx+2] = Rpt[2]
                    idx += 1
            
            #MatA = np.dot(A.T,A)
            #b = np.dot(A.T, b1)
            #xres = LA.tensorsolve(MatA, b)
            xres = spsolve(A.T*A, A.T*b1)
        
            print xres[len(self.FixedVertices):len(self.FixedVertices)+3]
            print self.BlendShapes[0].Vertices[int(self.InnerVertices[0])][0:3]
            
            ''' Affect result to mesh '''
            idx_in = len(self.FixedVertices)
            for idx in self.InnerVertices:
                self.BlendShapes[0].Vertices[int(idx),0] = xres[3*idx_in]
                self.BlendShapes[0].Vertices[int(idx),1] = xres[3*idx_in+1]
                self.BlendShapes[0].Vertices[int(idx),2] = xres[3*idx_in+2]
                self.BlendShapes[0].Normales[int(idx),0] = 0.
                self.BlendShapes[0].Normales[int(idx),1] = 0.
                self.BlendShapes[0].Normales[int(idx),2] = 0.
                idx_in +=1
                
        '''/***************Compute normals*******************/'''
        
        for face in self.InnerFaces:
            # Compute normal and weight of the face
            v1 = self.BlendShapes[0].Vertices[face[3]] - self.BlendShapes[0].Vertices[face[1]]
            v2 = self.BlendShapes[0].Vertices[face[5]] - self.BlendShapes[0].Vertices[face[1]]
            nmle = np.cross(v1[0:3],v2[0:3])
            nmle = nmle / LA.norm(nmle)
            
            self.BlendShapes[0].Normales[face[1], 0] = self.BlendShapes[0].Normales[face[1], 0] + nmle[0]
            self.BlendShapes[0].Normales[face[1], 1] = self.BlendShapes[0].Normales[face[1], 1] + nmle[1]
            self.BlendShapes[0].Normales[face[1], 2] = self.BlendShapes[0].Normales[face[1], 2] + nmle[2]
        
            self.BlendShapes[0].Normales[face[3], 0] = self.BlendShapes[0].Normales[face[3], 0] + nmle[0]
            self.BlendShapes[0].Normales[face[3], 1] = self.BlendShapes[0].Normales[face[3], 1] + nmle[1]
            self.BlendShapes[0].Normales[face[3], 2] = self.BlendShapes[0].Normales[face[3], 2] + nmle[2]
            
            self.BlendShapes[0].Normales[face[5], 0] = self.BlendShapes[0].Normales[face[5], 0] + nmle[0]
            self.BlendShapes[0].Normales[face[5], 1] = self.BlendShapes[0].Normales[face[5], 1] + nmle[1]
            self.BlendShapes[0].Normales[face[5], 2] = self.BlendShapes[0].Normales[face[5], 2] + nmle[2]
            
        for nmle in self.BlendShapes[0].Normales:  
            if (LA.norm(nmle) > 0.):
                nmle = nmle / LA.norm(nmle)
            
        #self.BlendShapes[0].Vertices = self.BlendShapes[0].T_Vertices.copy()
        #self.BlendShapes[0].Normales = self.BlendShapes[0].T_Normales.copy()
        
        '''/***********************Transfer expression deformation******************************/'''
        
        #boV = self.BlendShapes[0].Vertices.copy()
        boV = np.zeros(nb_col, dtype = np.float32)
        idx_in = 0
        for idx in self.FixedVertices:
            vtx = self.BlendShapes[0].Vertices[int(idx),:]
            boV[3*idx_in] = vtx[0]
            boV[3*idx_in+1] = vtx[1]
            boV[3*idx_in+2] = vtx[2]
            idx_in += 1
            
        for idx in self.InnerVertices:
            vtx = self.BlendShapes[0].Vertices[int(idx),:]
            boV[3*idx_in] = vtx[0]
            boV[3*idx_in+1] = vtx[1]
            boV[3*idx_in+2] = vtx[2]
            idx_in += 1
        
        FullList = [[int(3*i), int(3*i+1), int(3*i+2)] for i in self.FixedVertices] + [[int(3*i), int(3*i+1), int(3*i+2)] for i in self.InnerVertices]
        FullList = [i for sublist in FullList for i in sublist]
        
        indx_mesh = 0
        for mesh in self.BlendShapes[1::]:
            
            MatList1 = self.MatList1[indx_mesh][:, [int(i) for i in FullList]]
            MatList1 = MatList1[[int(i) for i in FullList],:]
            MatList2 = self.MatList2[indx_mesh][:, [int(i) for i in FullList]]
            MatList2 = MatList2[[int(i) for i in FullList],:]
            
            #xres = LA.tensorsolve(self.MatList1[indx_mesh], np.dot(self.MatList2[indx_mesh], boV))
            xres = spsolve(MatList1, MatList2 * boV)
            indx_mesh += 1
            
            ''' Affect result to mesh '''
            idx_in = len(self.FixedVertices)
            for idx in self.InnerVertices:
                mesh.Vertices[int(idx),0] = xres[3*idx_in]
                mesh.Vertices[int(idx),1] = xres[3*idx_in+1]
                mesh.Vertices[int(idx),2] = xres[3*idx_in+2]
                mesh.Normales[int(idx),0] = 0.
                mesh.Normales[int(idx),1] = 0.
                mesh.Normales[int(idx),2] = 0.
                idx_in += 1
                
            '''/***************Compute normals*******************/'''
            for face in self.InnerFaces:
                # Compute normal and weight of the face
                v1 = mesh.Vertices[face[3]] - mesh.Vertices[face[1]]
                v2 = mesh.Vertices[face[5]] - mesh.Vertices[face[1]]
                nmle = np.cross(v1[0:3],v2[0:3])
                nmle = nmle / LA.norm(nmle)
                
                mesh.Normales[face[1], 0] = mesh.Normales[face[1], 0] + nmle[0]
                mesh.Normales[face[1], 1] = mesh.Normales[face[1], 1] + nmle[1]
                mesh.Normales[face[1], 2] = mesh.Normales[face[1], 2] + nmle[2]
                
                mesh.Normales[face[3], 0] = mesh.Normales[face[3], 0] + nmle[0]
                mesh.Normales[face[3], 1] = mesh.Normales[face[3], 1] + nmle[1]
                mesh.Normales[face[3], 2] = mesh.Normales[face[3], 2] + nmle[2]
                
                mesh.Normales[face[5], 0] = mesh.Normales[face[5], 0] + nmle[0]
                mesh.Normales[face[5], 1] = mesh.Normales[face[5], 1] + nmle[1]
                mesh.Normales[face[5], 2] = mesh.Normales[face[5], 2] + nmle[2]
                
            for nmle in mesh.Normales:  
                if (LA.norm(nmle) > 0.):
                    nmle = nmle / LA.norm(nmle)

    ''' Generate / Update bump image '''
    def BumpImage(self, rgbd, Pose):
        
        self.Pose_d.set(Pose.astype(np.float32))
        intrinsic_depth = np.array([rgbd.intrinsic[0,0], rgbd.intrinsic[1,1], rgbd.intrinsic[0,2], rgbd.intrinsic[1,2]])
        self.intrinsic_d = cl.array.to_device(self.GPUManager.queue, intrinsic_depth)
        intrinsic_rgb = np.array([rgbd.intrinsicRGB[0,0], rgbd.intrinsicRGB[1,1], rgbd.intrinsicRGB[0,2], rgbd.intrinsicRGB[1,2]])
        self.intrinsic_RGB_d = cl.array.to_device(self.GPUManager.queue, intrinsic_rgb)
        
        self.Bump_Mapping(self.BumpImage_d, self.RGBBump_d, self.BumpSwap_d, self.RGBSwap_d, self.VMapBump_d, 
                          self.NMapBump_d, self.VerticesBS_d, self.BlendshapeCoeff_d, 
                          rgbd.vmap_d, rgbd.nmap_d, rgbd.color_d, self.Pose_d, self.Pose_D2RGB_d, 
                          self.intrinsic_d, self.intrinsic_RGB_d, self.Size[0], self.Size[1], self.SizeDraw[0], self.SizeDraw[1])
        
        #self.Impaint(self.BumpImage_d, self.RGBBump_d, self.BumpSwap_d, self.RGBSwap_d, self.Size[0], self.Size[1])
        self.BumpImage_d = self.BumpSwap_d.copy()
        self.RGBBump_d = self.RGBSwap_d.copy()
        
        self.vmap_to_nmap(self.VMapBump_d, self.NMapBump_d, self.Size[0], self.Size[1])
        
    def PreProcessing(self):
        for i in range(28):
            self.Vertices[i*4325:(i+1)*4325, ::] = self.BlendShapes[i].Vertices[::,0:3]
            self.Normales[i*4325:(i+1)*4325, ::] = self.BlendShapes[i].Normales[::]
        
        self.Vertices_d = cl.array.to_device(self.GPUManager.queue, self.Vertices)
        self.Normales_d = cl.array.to_device(self.GPUManager.queue, self.Normales)
        self.Triangles_d = cl.array.to_device(self.GPUManager.queue, self.BlendShapes[0].Faces)
        
        self.DataProc(self.BumpImage_d, self.WeightMap_d, self.Vertices_d, self.Normales_d, self.Triangles_d, self.VerticesBS_d, self.Size[0], self.Size[1])
        
    def VMapNMap(self):
        #self.BlendshapeCoeff_d[7] = 1.0
        self.vmap(self.VMapBump_d, self.NMapBump_d, self.VerticesBS_d, self.BlendshapeCoeff_d, self.Size[0], self.Size[1])
        
    
    def VMapExpression(self, Coeff, Pose):
        self.Pose_d.set(Pose.astype(np.float32))
        self.BlendshapeCoeff_d.set(Coeff.astype(np.float32))
        self.vmap_E(self.VMapBump_d, self.NMapBump_d, self.VerticesBS_d, self.BlendshapeCoeff_d, self.BumpImage_d, self.Pose_d, self.Size[0], self.Size[1])
        
    def NMap(self):
        #self.BumpSwap_d = self.BumpImage_d.copy()
        #self.Impaint(self.BumpImage_d, self.RGBBump_d, self.BumpSwap_d, self.RGBSwap_d, self.labels_d, self.Size[0], self.Size[1])
        
        #bump = self.BumpImage_d.get()
        #for i in range(len(self.BridgeList)):
        #    self.BumpImage_d[int(self.BridgeList[i][0][0]), int(self.BridgeList[i][0][1]),0] = 0.8*self.BumpImage_d[int(self.BridgeList[i][0][0]), int(self.BridgeList[i][0][1]),0] + 0.2*self.BumpImage_d[int(self.BridgeList[i][1][0]), int(self.BridgeList[i][1][1]),0]
            
        #self.BumpImage_d.set(bump)
            
        self.vmap_to_nmap(self.VMapBump_d, self.NMapBump_d, self.Size[0], self.Size[1])
        self.GPUManager.queue.finish()
        
    def Segment(self, rgbd, Pose):
        
        self.Pose_d.set(Pose.astype(np.float32))
        intrinsic_depth = np.array([rgbd.intrinsic[0,0], rgbd.intrinsic[1,1], rgbd.intrinsic[0,2], rgbd.intrinsic[1,2]])
        self.intrinsic_d = cl.array.to_device(self.GPUManager.queue, intrinsic_depth)
        intrinsic_rgb = np.array([rgbd.intrinsicRGB[0,0], rgbd.intrinsicRGB[1,1], rgbd.intrinsicRGB[0,2], rgbd.intrinsicRGB[1,2]])
        self.intrinsic_RGB_d = cl.array.to_device(self.GPUManager.queue, intrinsic_rgb)
        self.occluded_d.fill(0) 
        
         #head = img_as_float(self.color_image[self.rect.left():self.rect.right(), self.rect.top(): self.rect.bottom(), :])
        
        BBOX_d = cl.array.to_device(self.GPUManager.queue, np.array([10000, 0, 10000, 0], dtype = np.int32))
        self.bbox(self.VMapBump_d, BBOX_d, self.Pose_d, self.Pose_D2RGB_d, self.intrinsic_d, self.intrinsic_RGB_d)
        BBOX = BBOX_d.get()
        head = img_as_float(rgbd.color_image[BBOX[2]-20:BBOX[3]+20, BBOX[0]-40: BBOX[1]+40, :])
        
        segments = slic(head, n_segments = 200, sigma = 5)
        '''fig = plt.figure("Superpixels")
        ax = fig.add_subplot(1,1,1)
        ax.imshow(mark_boundaries(head, segments))
        plt.axis("off")
        
        plt.show()'''
        
        Mask = -np.ones((rgbd.Size[0], rgbd.Size[1]), dtype = np.int32)
        self.counter_segments = np.zeros(300, dtype = np.int32)
        
        for ((row,col),value) in np.ndenumerate(segments):
            Mask[row + BBOX[2]-20, col + BBOX[0]-40] = value
            self.counter_segments[value] += 1
            #print row, col, value
            
        rgbd.Mask_d.set(Mask)
        
        self.segment_occlusion(self.VMapBump_d, self.NMapBump_d, self.BumpImage_d, rgbd.Mask_d, self.occluded_d, rgbd.depth_d, self.Pose_d, self.Pose_D2RGB_d, self.intrinsic_d, self.intrinsic_RGB_d, rgbd.Size[0], rgbd.Size[1])
        
        OcclusionFlag = np.array([i > j/2 for (i,j) in zip(self.occluded_d.get(), self.counter_segments)])
        Mask = rgbd.color_image.copy()
        
        depth = rgbd.depth_d.get()
        for ((row,col),value) in np.ndenumerate(segments):
            if (OcclusionFlag[value]):
                Mask[row + BBOX[2]-20, col + BBOX[0]-40,0] = 0
                Mask[row + BBOX[2]-20, col + BBOX[0]-40,1] = 255
                Mask[row + BBOX[2]-20, col + BBOX[0]-40,2] = 0
                rgbd.color_image[row + BBOX[2]-20, col + BBOX[0]-40, 0] = 0
                rgbd.color_image[row + BBOX[2]-20, col + BBOX[0]-40, 1] = 0
                rgbd.color_image[row + BBOX[2]-20, col + BBOX[0]-40, 2] = 0
                depth[row + BBOX[2]-20, col + BBOX[0]-40] = 0.0
        
        cv2.imwrite("Mask.png", Mask)
        
        '''fig = plt.figure("Mask")
        ax = fig.add_subplot(1,1,1)
        ax.imshow(Mask)
        plt.axis("off")
        
        plt.show()'''
                
            
        #print OcclusionFlag
        self.occluded_d.set(OcclusionFlag.astype(np.int32))
        #print self.occluded_d.get()
        rgbd.depth_d.set(depth)
        rgbd.color_d.set(rgbd.color_image)
        
        self.fill(self.VMapBump_d, self.NMapBump_d, self.BumpImage_d, self.RGBBump_d, rgbd.Mask_d, self.occluded_d, rgbd.depth_d, rgbd.color_d, self.Pose_d, self.Pose_D2RGB_d, self.intrinsic_d, self.intrinsic_RGB_d, rgbd.Size[0], rgbd.Size[1])
        rgbd.color_image = rgbd.color_d.get()
        depth = rgbd.depth_d.get()
        
        tmpC = rgbd.color_image.copy()
        for ((row,col),value) in np.ndenumerate(segments):
            if (OcclusionFlag[value]): # and depth[row + BBOX[2]-20, col + BBOX[0]-40] == 0.0):
                val = 0.0
                count = 0.0
                s = 1
                while(count == 0.0 and s < 3):
                    val = 0.0
                    count = 0.0
                    for i in range(row + BBOX[2]-20-s,row + BBOX[2]-20+s+1):
                        for j in range(col + BBOX[0]-40 -s, col + BBOX[0]-40 + s+1):
                            w = 1.0 + LA.norm(np.array([i - row + BBOX[2]-20, j - col + BBOX[0]-40]))
                            if (depth[i,j] > 0.0):
                                val += depth[i,j]/w
                                count += 1.0/w
                    if (count > 0.0):
                        depth[row + BBOX[2]-20, col + BBOX[0]-40] = val/count  
                    s += 1
                    
            if (OcclusionFlag[value] and rgbd.color_image[row + BBOX[2]-20, col + BBOX[0]-40, 0] == 0):
                rgb = [0.0,0.0,0.0]
                countRGB = 0.0
                s = 1
                while(countRGB == 0.0 and s < 3):
                    rgb = [0.0,0.0,0.0]
                    countRGB = 0.0
                    for i in range(row + BBOX[2]-20-s,row + BBOX[2]-20+s+1):
                        for j in range(col + BBOX[0]-40 -s, col + BBOX[0]-40 + s+1):
                            w = LA.norm(np.array([i - row + BBOX[2]-20, j - col + BBOX[0]-40]))
                            if (tmpC[i,j,0] > 0.0 and w > 0.0):
                                rgb[0] = rgb[0] + float(tmpC[i,j,0])/w
                                rgb[1] = rgb[1] + float(tmpC[i,j,1])/w
                                rgb[2] = rgb[2] + float(tmpC[i,j,2])/w
                                countRGB += 1.0/w
                    if (countRGB > 0.0):
                        rgbd.color_image[row + BBOX[2]-20, col + BBOX[0]-40, 0] = int(rgb[0]/countRGB)
                        rgbd.color_image[row + BBOX[2]-20, col + BBOX[0]-40, 1] = int(rgb[1]/countRGB)
                        rgbd.color_image[row + BBOX[2]-20, col + BBOX[0]-40, 2] = int(rgb[2]/countRGB)
                    s += 1
        
        rgbd.depth_d.set(depth)
        
        
        #cv2.imwrite("Color.png", rgbd.color_image)
        
        '''fig = plt.figure("Color")
        ax = fig.add_subplot(1,1,1)
        ax.imshow(rgbd.color_image)
        plt.axis("off")
        
        plt.show()'''
        
        
    def DrawBump(self, Pose, intrinsic, color = 0):
        print "Draw bump image"
        self.Pose_d.set(Pose.astype(np.float32))
        intrinsic_curr = np.array([intrinsic[0,0], intrinsic[1,1], intrinsic[0,2], intrinsic[1,2]])
        self.intrinsic_d = cl.array.to_device(self.GPUManager.queue, intrinsic_curr)
        self.draw_d.set(self.draw_result)
        self.draw_vmap(self.VMapBump_d, self.NMapBump_d, self.RGBBump_d, self.draw_d, self.Pose_d, 
                       self.intrinsic_d, color, self.SizeDraw[0], self.SizeDraw[1])
        self.draw_buff_d = self.draw_d.copy()
        self.impaint_draw(self.draw_d, self.draw_buff_d, 1.0, self.SizeDraw[0], self.SizeDraw[1])
        self.GPUManager.queue.finish()
        return self.draw_d.get()
    
    def DrawBumpMesh(self, Pose, intrinsic, canvas, color = 0):
        vmap = self.VMapBump_d.get()
        nmap = self.NMapBump_d.get()
        rgbmap = self.RGBBump_d.get()
        
        pix = np.array([0.0,0.0,1.0])
        for i in range(1,self.Size[0]-1):
            for j in range(1,self.Size[1]-1):
                if (nmap[i,j,2] == 0.0 or nmap[i,j+1,2] == 0.0 or nmap[i+1,j,2] == 0.0):
                    continue
                
                max_dist = max(max(LA.norm(vmap[i,j,:] - vmap[i,j+1, :]), LA.norm(vmap[i,j,:] - vmap[i+1,j, :])), LA.norm(vmap[i+1,j,:] - vmap[i,j+1, :]))
                
                if (max_dist > 0.01):
                    continue
                
                inviewingvolume = False
                poly = []
                pt = np.array([vmap[i,j,0], vmap[i,j,1], vmap[i,j,2], 1.0])
                pt = np.dot(Pose, pt)
                nmle = np.array([nmap[i,j,0], nmap[i,j,1], nmap[i,j,2]])
                nmle = np.dot(Pose[0:3,0:3], nmle)
                nmle_tot = nmle.copy()
                pix[0] = pt[0]/pt[2]
                pix[1] = pt[1]/pt[2]
                pix = np.dot(intrinsic, pix)
                column_index = int(round(pix[0]))
                line_index = int(round(pix[1]))
                poly.append((column_index, line_index))
                    
                if (column_index > -1 and column_index < self.Size[1] and line_index > -1 and line_index < self.Size[0] and pt[2] > 0. and nmle[2] < 0.):
                    inviewingvolume = True
                
                pt = np.array([vmap[i,j+1,0], vmap[i,j+1,1], vmap[i,j+1,2], 1.0])
                pt = np.dot(Pose, pt)
                nmle = np.array([nmap[i,j+1,0], nmap[i,j+1,1], nmap[i,j+1,2]])
                nmle = np.dot(Pose[0:3,0:3], nmle)
                nmle_tot = nmle_tot+nmle
                pix[0] = pt[0]/pt[2]
                pix[1] = pt[1]/pt[2]
                pix = np.dot(intrinsic, pix)
                column_index = int(round(pix[0]))
                line_index = int(round(pix[1]))
                poly.append((column_index, line_index))
                    
                if (column_index > -1 and column_index < self.Size[1] and line_index > -1 and line_index < self.Size[0] and pt[2] > 0. and nmle[2] < 0.):
                    inviewingvolume = True
                
                pt = np.array([vmap[i+1,j,0], vmap[i+1,j,1], vmap[i+1,j,2], 1.0])
                pt = np.dot(Pose, pt)
                nmle = np.array([nmap[i+1,j,0], nmap[i+1,j,1], nmap[i+1,j,2]])
                nmle = np.dot(Pose[0:3,0:3], nmle)
                nmle_tot = nmle_tot+nmle
                pix[0] = pt[0]/pt[2]
                pix[1] = pt[1]/pt[2]
                pix = np.dot(intrinsic, pix)
                column_index = int(round(pix[0]))
                line_index = int(round(pix[1]))
                poly.append((column_index, line_index))
                    
                if (column_index > -1 and column_index < self.Size[1] and line_index > -1 and line_index < self.Size[0] and pt[2] > 0. and nmle[2] < 0.):
                    inviewingvolume = True
            
                if (color == 0):
                    nmle_tot = (nmle_tot/LA.norm(nmle_tot))*200.0
                    r = str(hex(int(abs(nmle_tot[2]))))[2::] 
                    if (len(r) == 1):
                        r = '0'+r
                    g = str(hex(int(abs(nmle_tot[2]))))[2::] 
                    if (len(g) == 1):
                        g = '0'+g
                    b = str(hex(int(abs(nmle_tot[2]))))[2::] 
                    if (len(b) == 1):
                        b = '0'+ b
                    arg = '#' + r + g + b
                else:
                    clr = (rgbmap[i,j,:] + rgbmap[i,j+1,:] + rgbmap[i+1,j,:])/3.0
                    r = str(hex(int(clr[2])))[2::] 
                    if (len(r) == 1):
                        r = '0'+r
                    g = str(hex(int(clr[1])))[2::] 
                    if (len(g) == 1):
                        g = '0'+g
                    b = str(hex(int(clr[0])))[2::] 
                    if (len(b) == 1):
                        b = '0'+ b
                    arg = '#' + r + g + b
                    
                if inviewingvolume:
                    canvas.create_polygon(*poly, fill=arg)
                
                #################################
                    
                if (nmap[i,j+1,2] == 0.0 or nmap[i+1,j+1,2] == 0.0 or nmap[i+1,j,2] == 0.0):
                    continue
                
                
                max_dist = max(max(LA.norm(vmap[i,j+1,:] - vmap[i+1,j+1, :]), LA.norm(vmap[i,j+1,:] - vmap[i+1,j, :])), LA.norm(vmap[i+1,j,:] - vmap[i+1,j+1, :]))
                
                if (max_dist > 0.01):
                    continue
                
                inviewingvolume = False
                poly = []
                pt = np.array([vmap[i,j+1,0], vmap[i,j+1,1], vmap[i,j+1,2], 1.0])
                pt = np.dot(Pose, pt)
                nmle = np.array([nmap[i,j+1,0], nmap[i,j+1,1], nmap[i,j+1,2]])
                nmle = np.dot(Pose[0:3,0:3], nmle)
                nmle_tot = nmle.copy()
                pix[0] = pt[0]/pt[2]
                pix[1] = pt[1]/pt[2]
                pix = np.dot(intrinsic, pix)
                column_index = int(round(pix[0]))
                line_index = int(round(pix[1]))
                poly.append((column_index, line_index))
                    
                if (column_index > -1 and column_index < self.Size[1] and line_index > -1 and line_index < self.Size[0] and pt[2] > 0. and nmle[2] < 0.):
                    inviewingvolume = True
                
                pt = np.array([vmap[i+1,j+1,0], vmap[i+1,j+1,1], vmap[i+1,j+1,2], 1.0])
                pt = np.dot(Pose, pt)
                nmle = np.array([nmap[i+1,j+1,0], nmap[i+1,j+1,1], nmap[i+1,j+1,2]])
                nmle = np.dot(Pose[0:3,0:3], nmle)
                nmle_tot = nmle_tot+nmle
                pix[0] = pt[0]/pt[2]
                pix[1] = pt[1]/pt[2]
                pix = np.dot(intrinsic, pix)
                column_index = int(round(pix[0]))
                line_index = int(round(pix[1]))
                poly.append((column_index, line_index))
                    
                if (column_index > -1 and column_index < self.Size[1] and line_index > -1 and line_index < self.Size[0] and pt[2] > 0. and nmle[2] < 0.):
                    inviewingvolume = True
                
                pt = np.array([vmap[i+1,j,0], vmap[i+1,j,1], vmap[i+1,j,2], 1.0])
                pt = np.dot(Pose, pt)
                nmle = np.array([nmap[i+1,j,0], nmap[i+1,j,1], nmap[i+1,j,2]])
                nmle = np.dot(Pose[0:3,0:3], nmle)
                nmle_tot = nmle_tot+nmle
                pix[0] = pt[0]/pt[2]
                pix[1] = pt[1]/pt[2]
                pix = np.dot(intrinsic, pix)
                column_index = int(round(pix[0]))
                line_index = int(round(pix[1]))
                poly.append((column_index, line_index))
                    
                if (column_index > -1 and column_index < self.Size[1] and line_index > -1 and line_index < self.Size[0] and pt[2] > 0. and nmle[2] < 0.):
                    inviewingvolume = True
            
                
                if (color == 0):
                    nmle_tot = (nmle_tot/LA.norm(nmle_tot))*200.0
                    r = str(hex(int(abs(nmle_tot[2]))))[2::] 
                    if (len(r) == 1):
                        r = '0'+r
                    g = str(hex(int(abs(nmle_tot[2]))))[2::] 
                    if (len(g) == 1):
                        g = '0'+g
                    b = str(hex(int(abs(nmle_tot[2]))))[2::] 
                    if (len(b) == 1):
                        b = '0'+ b
                    arg = '#' + r + g + b
                else:
                    clr = (rgbmap[i+1,j+1,:] + rgbmap[i,j+1,:] + rgbmap[i+1,j,:])/3.0
                    r = str(hex(int(clr[2])))[2::] 
                    if (len(r) == 1):
                        r = '0'+r
                    g = str(hex(int(clr[1])))[2::] 
                    if (len(g) == 1):
                        g = '0'+g
                    b = str(hex(int(clr[0])))[2::] 
                    if (len(b) == 1):
                        b = '0'+ b
                    arg = '#' + r + g + b
                    
                if inviewingvolume:
                    canvas.create_polygon(*poly, fill=arg)
                
    
    
    def SetLandmarks(self, rgbd):
        self.LandmarksBump = np.zeros((51,2), dtype = np.int32)
        for i in range(51):
            best_i = self.BlendShapes[0].Vertices[FACIAL_LANDMARKS[i], 4]*self.Size[0]
            best_j = self.BlendShapes[0].Vertices[FACIAL_LANDMARKS[i], 5]*self.Size[1]
            
            if (i == 22):
                best_i = 110
                best_j = 169
            if (i == 25):
                best_i = 130
                best_j = 169
                
            self.LandmarksBump[i] = (best_i, best_j)
            
        rgb2depth = rgbd.RGB2Depth.get()
        vmap = rgbd.vmap_d.get()
        for i in range(51):
            # search for vertex index closest to the 44 landmark
            idx = rgb2depth[rgbd.shape[i][1], rgbd.shape[i][0]]
            if (idx == 0):
                print "No landmark"
                
            landmark = vmap[idx/rgbd.Size[1], idx%rgbd.Size[1],:]
            min_dist = 10000.0
            best_idx = 0
            curr_i = 0
            for v in self.BlendShapes[0].Vertices:
                dist = LA.norm(v[0:3]-landmark)
                if (min_dist > dist):
                    min_dist = dist
                    best_idx = curr_i
                curr_i += 1
            FACIAL_LANDMARKS[i] = best_idx
            #print "index for landmark 43: ", best_idx 
        
        '''# search for vertex index closest to the 48 landmark
        idx = rgb2depth[rgbd.shape[47][1], rgbd.shape[47][0]]
        if (idx == 0):
            print "No landmark"
            
        landmark = vmap[idx/rgbd.Size[1], idx%rgbd.Size[1],:]
        min_dist = 10000.0
        best_idx = 0
        curr_i = 0
        for v in self.BlendShapes[0].Vertices:
            dist = LA.norm(v[0:3]-landmark)
            if (min_dist > dist):
                min_dist = dist
                best_idx = curr_i
            curr_i += 1
        print "index for landmark 47: ", best_idx'''
            
    def Save(self, dest):
        print "recording result at: ", dest
        
        self.BlendShapes[0].SaveMesh()
        
        vmap = self.VMapBump_d.get()
        nmap = self.NMapBump_d.get()
        rgb = self.RGBBump_d.get()
        bump = (self.BumpImage_d.get()).reshape((400,400,3))
        
        cv2.imwrite(dest+'/Bump_RGB_'+str(self.indexSave) + '.png',rgb.astype(np.uint8))
        cv2.imwrite(dest+'/Bump_'+str(self.indexSave) + '.png', 50*(bump[:,:,0]+5.0)*(bump[:,:,1] == 0.0))  

        '''rgb = np.zeros((self.Size[0], self.Size[1], 3), dtype = np.int32)   
        
        for vtx in self.FixedVertices:
            rgb[int(round(self.BlendShapes[0].Vertices[int(vtx),4]*self.Size[0])), int(round(self.BlendShapes[0].Vertices[int(vtx),5]*self.Size[1])),:] = [255,0,0]
        
        for vtx in self.InnerVertices:
            rgb[int(round(self.BlendShapes[0].Vertices[int(vtx),4]*self.Size[0])), int(round(self.BlendShapes[0].Vertices[int(vtx),5]*self.Size[1])),:] = [0,0,255]'''
        
        # Write vertices
        Index = np.zeros((self.Size[0], self.Size[1]), dtype = np.int32)
        curr_ind = 0
        Vertices = []
        Normales = []
        Colors = []
        for i in range(self.Size[0]):
            for j in range(self.Size[1]):
                if (vmap[i,j,2] != 0.0):
                    Index[i,j] = curr_ind
                    Vertices.append([vmap[i,j,0], vmap[i,j,1], vmap[i,j,2]])
                    Normales.append([nmap[i,j,0], nmap[i,j,1], nmap[i,j,2]])
                    Colors.append([int(rgb[i,j,2]), int(rgb[i,j,1]), int(rgb[i,j,0])])
                    curr_ind += 1
                
        # Write the faces
        faces = []
        for i in range(self.Size[0]-1):
            for j in range(self.Size[1]-1):
                # triangle 1
                if (vmap[i,j,2] != 0.0 and vmap[i+1,j,2] != 0.0 and vmap[i,j+1,2] != 0.0):
                    faces.append([Index[i,j], Index[i+1,j], Index[i,j+1]])
                #triangle 2
                if (vmap[i,j+1,2] != 0.0 and vmap[i+1,j,2] != 0.0 and vmap[i+1,j+1,2] != 0.0):
                    faces.append([Index[i,j+1], Index[i+1,j], Index[i+1,j+1]])
                   
        
        ''' Write result 3D mesh into a .ply file'''
        f = open(dest+'/Mesh_'+str(self.indexSave) + '.ply', 'wb')
        
        # Write headers
        f.write("ply\n")
        f.write("format ascii 1.0\n")
        f.write("comment ply file created by Diego Thomas\n")
        f.write("element vertex %d \n" %(len(Vertices)))
        f.write("property float x\n")
        f.write("property float y\n")
        f.write("property float z\n")
        f.write("property float nx\n")
        f.write("property float ny\n")
        f.write("property float nz\n")
        f.write("property uchar red\n")
        f.write("property uchar green\n")
        f.write("property uchar blue\n")
        f.write("element face %d \n" %(len(faces)))
        f.write("property list uchar int vertex_indices\n")
        f.write("end_header\n")
        
        
        for (v,n,r) in zip(Vertices, Normales, Colors):
            f.write("%f %f %f %f %f %f %d %d %d\n" %(v[0], v[1], v[2], 
                                                n[0], n[1], n[2],
                                                r[0], r[1], r[2]))
            
        for face_curr in faces:
            f.write("3 %d %d %d \n" %(face_curr[0], face_curr[1], face_curr[2])) 
        
        f.close()
        
        self.indexSave += 1
            
