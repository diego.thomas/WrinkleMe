#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 11 17:42:12 2017

@author: diegothomas
"""

import numpy as np
import Tkinter as tk
import imp
import time
from PIL import Image
from PIL import ImageTk
from math import cos, sin

from os import path
APP_ROOT = path.dirname( path.abspath( __file__ ) )
RGBD = imp.load_source('RGBD', APP_ROOT + '/RGBD.py')
GPU = imp.load_source('GPUManager', APP_ROOT + '/GPUManager.py')

'''
    Manager for the calibration application
'''
class Calibration(tk.Frame):
    
    ''' Function to handle keyboard inputs '''
    def key(self, event):
        # 3D transformation that will take the deplacement due to keyboard input
        # Initialise as a 4x4 identity matrix
        Transfo = np.array([[1., 0., 0., 0.], 
                            [0., 1., 0., 0.], 
                            [0., 0., 1., 0.], 
                            [0., 0., 0., 1.]])
        
        if (event.keysym == 'Escape'): # the escape key is hit => exit the application
            self.root.destroy()
        
        '''
            'd' = shift to the right
            'a' = shift to the left
            'w' = move forward
            's' = move backward
            'q' = move up
            'e' = move down
            'c' = switch color mode
            'b' = Record result
        '''
        if (event.keysym == 'd'):
            Transfo[0,3] = 0.01
        if (event.keysym == 'a'):
            Transfo[0,3] = -0.01
        if (event.keysym == 'w'):
            Transfo[1,3] = -0.01
        if (event.keysym == 's'):
            Transfo[1,3] = 0.01
        if (event.keysym == 'e'):
            Transfo[2,3] = -0.01
        if (event.keysym == 'q'):
            Transfo[2,3] = 0.01
        if (event.keysym == 'c'):
            self.color_tag = (self.color_tag+1) %3
            if (self.color_tag == 1):
                self.color_tag = 2
        if (event.keysym == 'b'):
            self.SaveResult()
        if (event.keysym == 'r'):
            self.GetNewData()
            
        # if the application is still running: do some drawing
        if (event.keysym != 'Escape'):
            # Update the current global camera pose matrix
            self.Pose = np.dot(self.Pose, Transfo)
            self.res = self.RGBD.Draw_GPU(self.Pose,1,self.color_tag).astype(np.uint8)
            self.res = Image.fromarray(self.res, 'RGB')
            self.res = ImageTk.PhotoImage(self.res)
            self.canvas.create_image(320,240,image = self.res)
            
    def SaveResult(self):
        print "Intrinsic depth: ", self.intrinsicDepth
        print "Intrinsic color: ",self.intrinsicRGB
        print "Extrinsic Depth-Color: ",self.D2RGB 
        
        
    ''' Function to handle mouse press event '''
    def mouse_press(self, event):
        self.x_init = event.x
        self.y_init = event.y
    
    ''' Function to handle mouse motion events '''
    def mouse_motion(self, event):
        if (event.y < self.Size[0] and event.x < self.Size[1]):
            '''delta_x = event.x - self.x_init
            delta_y = event.y - self.y_init
            
            angley = 0.
            if (delta_x > 0.):
                angley = -0.01
            elif (delta_x < 0.):
                angley = 0.01 #pi * 2. * delta_x / float(self.Size[0])
            RotY = np.array([[cos(angley), 0., sin(angley), 0.], \
                             [0., 1., 0., 0.], \
                             [-sin(angley), 0., cos(angley), 0.], \
                             [0., 0., 0., 1.]])
            self.Pose = np.dot(self.Pose, RotY)
            
            anglex = 0.
            if (delta_y > 0.):
                anglex = 0.01
            elif (delta_y < 0.):
                anglex = -0.01 # pi * 2. * delta_y / float(self.Size[0])
            RotX = np.array([[1., 0., 0., 0.], \
                            [0., cos(anglex), -sin(anglex), 0.], \
                            [0., sin(anglex), cos(anglex), 0.], \
                            [0., 0., 0., 1.]])

            self.Pose = np.dot(self.Pose, RotX)'''
            
            # Read new values for depth intrinsics
            self.intrinsicDepth[0,0] = self.intrinsic[0,0] + (float(self.w_Depth_1.get())-150.0)/2.
            self.intrinsicDepth[0,2] = self.intrinsic[0,2] + (float(self.w_Depth_2.get())-150.0)/2.
            self.intrinsicDepth[1,1] = self.intrinsic[1,1] + (float(self.w_Depth_3.get())-150.0)/2.
            self.intrinsicDepth[1,2] = self.intrinsic[1,2] + (float(self.w_Depth_4.get())-150.0)/2.
        
            # Read new values for color intrinsics
            self.intrinsicRGB[0,0] = self.intrinsicRGB_init[0,0] + (float(self.w_RGB_1.get())-150.0)/2.
            self.intrinsicRGB[0,2] = self.intrinsicRGB_init[0,2] + (float(self.w_RGB_2.get())-150.0)/2.
            self.intrinsicRGB[1,1] = self.intrinsicRGB_init[1,1] + (float(self.w_RGB_3.get())-150.0)/2.
            self.intrinsicRGB[1,2] = self.intrinsicRGB_init[1,2] + (float(self.w_RGB_4.get())-150.0)/2.
            
            # Read new values for alignment
            '''RotX = np.array([[1., 0., 0., 0.], \
                            [0., cos((float(self.w_Calib_1.get())-50.0)/50.0), -sin((float(self.w_Calib_1.get())-50.0)/50.0), 0.], \
                            [0., sin((float(self.w_Calib_1.get())-50.0)/50.0), cos((float(self.w_Calib_1.get())-50.0)/50.0), 0.], \
                            [0., 0., 0., 1.]])
            
            RotY = np.array([[cos((float(self.w_Calib_2.get())-50.0)/50.0), 0., sin((float(self.w_Calib_2.get())-50.0)/50.0), 0.], \
                             [0., 1., 0., 0.], \
                             [-sin((float(self.w_Calib_2.get())-50.0)/50.0), 0., cos((float(self.w_Calib_2.get())-50.0)/50.0), 0.], \
                             [0., 0., 0., 1.]])
    
            RotZ = np.array([[cos((float(self.w_Calib_3.get())-50.0)/50.0), sin((float(self.w_Calib_3.get())-50.0)/50.0), 0. , 0.], \
                             [-sin((float(self.w_Calib_3.get())-50.0)/50.0), cos((float(self.w_Calib_3.get())-50.0)/50.0), 0., 0.], \
                             [0., 0., 1., 0.], \
                             [0., 0., 0., 1.]])'''
    
            #self.D2RGB = np.dot(RotZ, np.dot(RotY, RotX))
            self.D2RGB[0,3] = 0.05 + (float(self.w_Calib_4.get())-50.0)/500.0
            '''self.D2RGB[1,3] = (float(self.w_Calib_5.get())-50.0)/500.0
            self.D2RGB[2,3] = (float(self.w_Calib_6.get())-50.0)/500.0'''
            
            self.RGBD.intrinsic = self.intrinsicDepth
            self.RGBD.intrinsicRGB = self.intrinsicRGB
            self.RGBD.Calib = self.D2RGB
            
            self.RGBD.Vmap_GPU(0)
            self.RGBD.ReProj_depth(self.D2RGB)
            #self.RGBD.Vmap_GPU(1)
            self.RGBD.NMap_GPU()
            
    
            self.res = self.RGBD.Draw_GPU(self.Pose,1,self.color_tag).astype(np.uint8)
            self.res = Image.fromarray(self.res, 'RGB')
            self.res = ImageTk.PhotoImage(self.res)
            self.canvas.create_image(320,240,image = self.res)
            
            
        self.x_init = event.x
        self.y_init = event.y
        
    def GetNewData(self):
        self.RGBD.depthname = self.path + '/Depth_'+str(self.RGBD.index)+'.tiff'
        self.RGBD.colorname = self.path + '/RGB_'+str(self.RGBD.index)+'.tiff'
        self.RGBD.index += 1
        
        if (not self.RGBD.ReadFromDisk()):
            return False
        
        self.RGBD.BilateralFilter_GPU(2,0.02,3)
        
        self.RGBD.Vmap_GPU(0)
        self.RGBD.ReProj_depth(self.D2RGB)
        
        self.RGBD.NMap_GPU()
        
        self.GPUManager.queue.finish()
        
        return True
    
    ''' Constructor function '''
    def __init__(self, path, GPUManager, master=None):
        # link with the global Tkinter app
        self.root = master
        # Initialise path to load data
        self.path = path
        # Initialise GPU manager class
        self.GPUManager = GPUManager
        # Tag do draw or not colors
        self.color_tag = 2
        # Initialise camera pose (transfo from ref to curr frame) to 4x4 identity matrix
        self.Pose = np.array([[1., 0., 0., 0.], 
                              [0., 1., 0., 0.], 
                              [0., 0., 1., 0.], 
                              [0., 0., 0., 1.]], dtype = np.float32)
    
        self.D2RGB = np.array([[1., 0., 0., 0.], 
                              [0., 1., 0., 0.], 
                              [0., 0., 1., 0.], 
                              [0., 0., 0., 1.]], dtype = np.float32)
        
        # Initialise tk rendering object
        tk.Frame.__init__(self, master)
        self.pack()
        
        # Read the calibration file to get camera intrinsic parameters
        calib_file = open(self.path + '/Calib.txt', 'r')
        calib_data = calib_file.readlines()
        self.Size = [int(calib_data[0]), int(calib_data[1])]
        self.intrinsicRGB_init = np.array([[float(calib_data[2]), float(calib_data[3]), float(calib_data[4])], \
                                   [float(calib_data[5]), float(calib_data[6]), float(calib_data[7])], \
                                   [float(calib_data[8]), float(calib_data[9]), float(calib_data[10])]], dtype = np.float32)
    
        self.intrinsic = np.array([[float(calib_data[12]), float(calib_data[13]), float(calib_data[14])], \
                                   [float(calib_data[15]), float(calib_data[16]), float(calib_data[17])], \
                                   [float(calib_data[18]), float(calib_data[19]), float(calib_data[20])]], dtype = np.float32)
    
        self.D2RGB = np.array([[float(calib_data[21]), float(calib_data[22]), float(calib_data[23]), float(calib_data[24])], 
                              [float(calib_data[25]), float(calib_data[26]), float(calib_data[27]), float(calib_data[28])], 
                              [float(calib_data[29]), float(calib_data[30]), float(calib_data[31]), float(calib_data[32])], 
                              [float(calib_data[33]), float(calib_data[34]), float(calib_data[35]), float(calib_data[36])]], dtype = np.float32)
    
        self.intrinsicDepth = np.identity(3, dtype = np.float32)
        self.intrinsicRGB = np.identity(3, dtype = np.float32)
        
        self.fact = float(calib_data[11])
        self.SizeRGB = [int(calib_data[37]), int(calib_data[38])]
        print "Input depth image size: \n", self.Size
        print "Input RGB image size: \n", self.SizeRGB
        print "Camera intrinsic matrix: \n", self.intrinsic
        print "Input depth encoding factor: ", self.fact
        
        # Visualisation camera image
        self.canvas = tk.Canvas(self, bg="black", height=self.Size[0], width=self.Size[1])
        self.canvas.pack()  
        
        # Create the object that will manage the RGBD data
        self.RGBD = RGBD.RGBD(self.GPUManager, self.path + '/Depth_10.tiff', self.path + '/RGB_10.tiff', self.intrinsic, (self.Size[0], self.Size[1], 3), self.fact, (self.SizeRGB[0], self.SizeRGB[1], 3))
        self.RGBD.index = 10
        self.RGBD.intrinsicRGB = self.intrinsicRGB_init
        
        start_time = time.time()
        self.RGBD.ReadFromDisk()
        elapsed_time = time.time() - start_time
        print "ReadFromDisk: %f" % (elapsed_time)
        
        start_time = time.time()
        self.RGBD.BilateralFilter_GPU(2,0.02,3)
        elapsed_time = time.time() - start_time
        print "BilateralFilter: %f" % (elapsed_time)
        
        start_time = time.time()
        self.RGBD.Vmap_GPU()
        elapsed_time = time.time() - start_time
        print "Vmap: %f" % (elapsed_time)
        
        self.res = self.RGBD.Draw_GPU(self.Pose,1,self.color_tag).astype(np.uint8)
        self.res = Image.fromarray(self.res, 'RGB')
        self.res = ImageTk.PhotoImage(self.res)
        self.canvas.create_image(320,240,image =  self.res)
        
        #enable keyboard and mouse monitoring
        self.root.bind("<Key>", self.key)
        self.root.bind("<Button-1>", self.mouse_press)
        self.root.bind("<B1-Motion>", self.mouse_motion)

        # sliders for 4 RGB intrinsics parameters
        self.w_RGB_1 = tk.Scale(master, from_=0, to=300, length = 640, orient=tk.HORIZONTAL)
        self.w_RGB_2 = tk.Scale(master, from_=0, to=300, length = 640, orient=tk.HORIZONTAL)
        self.w_RGB_3 = tk.Scale(master, from_=0, to=300, length = 640, orient=tk.HORIZONTAL)
        self.w_RGB_4 = tk.Scale(master, from_=0, to=300, length = 640, orient=tk.HORIZONTAL)
        self.w_RGB_1.set(150)
        self.w_RGB_2.set(150)
        self.w_RGB_3.set(150)
        self.w_RGB_4.set(150)
        self.w_RGB_1.pack()
        self.w_RGB_2.pack()
        self.w_RGB_3.pack()
        self.w_RGB_4.pack()
        
        # sliders for 4 depth intrinsics parameters
        self.w_Depth_1 = tk.Scale(master, from_=0, to=300, length = 640, orient=tk.HORIZONTAL)
        self.w_Depth_2 = tk.Scale(master, from_=0, to=300, length = 640, orient=tk.HORIZONTAL)
        self.w_Depth_3 = tk.Scale(master, from_=0, to=300, length = 640, orient=tk.HORIZONTAL)
        self.w_Depth_4 = tk.Scale(master, from_=0, to=300, length = 640, orient=tk.HORIZONTAL)
        self.w_Depth_1.set(150)
        self.w_Depth_2.set(150)
        self.w_Depth_3.set(150)
        self.w_Depth_4.set(150)
        self.w_Depth_1.pack()
        self.w_Depth_2.pack()
        self.w_Depth_3.pack()
        self.w_Depth_4.pack()
        
        # sliders for 6 3D Align parameters
        '''self.w_Calib_1 = tk.Scale(master, from_=1, to=100, length = 320, orient=tk.HORIZONTAL)
        self.w_Calib_2 = tk.Scale(master, from_=1, to=100, length = 320, orient=tk.HORIZONTAL)
        self.w_Calib_3 = tk.Scale(master, from_=1, to=100, length = 320, orient=tk.HORIZONTAL)'''
        self.w_Calib_4 = tk.Scale(master, from_=1, to=100, length = 320, orient=tk.HORIZONTAL)
        '''self.w_Calib_5 = tk.Scale(master, from_=1, to=100, length = 320, orient=tk.HORIZONTAL)
        self.w_Calib_6 = tk.Scale(master, from_=1, to=100, length = 320, orient=tk.HORIZONTAL)
        self.w_Calib_1.set(50)
        self.w_Calib_2.set(50)
        self.w_Calib_3.set(50)'''
        self.w_Calib_4.set(50)
        '''self.w_Calib_5.set(50)
        self.w_Calib_6.set(50)
        self.w_Calib_1.pack()
        self.w_Calib_2.pack()
        self.w_Calib_3.pack()'''
        self.w_Calib_4.pack()
        '''self.w_Calib_5.pack()
        self.w_Calib_6.pack()'''
        
        self.root.mainloop()