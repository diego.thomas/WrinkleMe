#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 26 11:10:29 2017

@author: diegothomas
"""

import cv2
import numpy as np
from numpy import linalg as LA
from scipy.optimize import least_squares
import math
import imp
from os import path


FACIAL_LANDMARKS = [3749, 3745, 3731, 3725, 3704, 1572, 1592, 1598, 1612, 1617, 3662, 
                    2207, 3093, 966, 2650, 2774, 2693, 662, 558, 2345, 2336, 2369, 
                    4266, 2237, 2298, 2096, 1981, 1980, 1978, 237, 181, 2838, 4307, 
                    4304, 4301, 4298, 4295, 732, 996, 998, 4277, 3148, 3133, 2865, 
                    2728, 2188,  629, 733, 1011, 3114, 3110]

class NonRigidRegistration():
    
    ''' Constructor '''
    def __init__(self, rgbd, Mesh):
        self.max_iter = 10
        self.rgbd = rgbd
        self.rgb2depth = rgbd.RGB2Depth.get()
        self.vmap = rgbd.vmap_d.get()
        self.nmap = rgbd.nmap_d.get()
        self.intrinsic = rgbd.intrinsic
        self.Mesh = Mesh
        
        
    def Smoothness(self, deformed_vertices):
        res = []
        face_idx = 0
        for face in self.Mesh.Faces:
            v1 = self.Mesh.Vertices[face[0],0:3]; v2 = self.Mesh.Vertices[face[2],0:3]; v3 = self.Mesh.Vertices[face[4],0:3]
            v4 = v1 + np.cross(v2-v1,v3-v1)/math.sqrt(LA.norm(np.cross(v2-v1,v3-v1)))
            v1_d = deformed_vertices[3*face[0]:3*face[0]+3]; v2_d = deformed_vertices[3*face[2]:3*face[2]+3]; v3_d = deformed_vertices[3*face[4]:3*face[4]+3]
            v4_d = v1_d + np.cross(v2_d-v1_d,v3_d-v1_d)/math.sqrt(LA.norm(np.cross(v2_d-v1_d,v3_d-v1_d)))
            V = [v2 - v1, v3 - v1, v4 - v1]
            V_d = [v2_d - v1_d, v3_d - v1_d, v4_d - v1_d]
            T_i = np.dot(V_d, LA.inv(V))
            for adj in self.Mesh.AdjFaces[face_idx]:
                v1 = self.Mesh.Vertices[adj[0],0:3]; v2 = self.Mesh.Vertices[adj[2],0:3]; v3 = self.Mesh.Vertices[adj[4],0:3]
                v4 = v1 + np.cross(v2-v1,v3-v1)/math.sqrt(LA.norm(np.cross(v2-v1,v3-v1)))
                v1_d = deformed_vertices[3*adj[0]:3*adj[0]+3]; v2_d = deformed_vertices[3*adj[2]:3*adj[2]+3]; v3_d = deformed_vertices[3*adj[4]:3*adj[4]+3]
                v4_d = v1_d + np.cross(v2_d-v1_d,v3_d-v1_d)/math.sqrt(LA.norm(np.cross(v2_d-v1_d,v3_d-v1_d)))
                V = [v2 - v1, v3 - v1, v4 - v1]
                V_d = [v2_d - v1_d, v3_d - v1_d, v4_d - v1_d]
                T_j = np.dot(V_d, LA.inv(V))
                res = np.hstack((res,(T_i - T_j).flatten()))
            face_idx +=1
        
        return np.array(res)
    
    def Smoothness_GPU(self, deformed_vertices):
        res = []
        
        return res
    
    def Identity(self, deformed_vertices):
        res = []
        for face in self.Mesh.Faces:
            v1 = self.Mesh.Vertices[face[0],0:3]; v2 = self.Mesh.Vertices[face[2],0:3]; v3 = self.Mesh.Vertices[face[4],0:3]
            v4 = v1 + np.cross(v2-v1,v3-v1)/math.sqrt(LA.norm(np.cross(v2-v1,v3-v1)))
            v1_d = deformed_vertices[3*face[0]:3*face[0]+3]; v2_d = deformed_vertices[3*face[2]:3*face[2]+3]; v3_d = deformed_vertices[3*face[4]:3*face[4]+3]
            v4_d = v1_d + np.cross(v2_d-v1_d,v3_d-v1_d)/math.sqrt(LA.norm(np.cross(v2_d-v1_d,v3_d-v1_d)))
            V = [v2 - v1, v3 - v1, v4 - v1]
            V_d = [v2_d - v1_d, v3_d - v1_d, v4_d - v1_d]
            T_i = np.dot(V_d, LA.inv(V))
            res = np.hstack((res, (T_i - np.identity(3)).flatten()))
            
        return np.array(res)
    
    def Correspondences(self, deformed_vertices):
        res = []
        s = 5
        pix = np.array([0., 0., 1.])
        n = len(self.Mesh.Vertices)
        for i in range(n):
            vertex = np.array([deformed_vertices[3*i], deformed_vertices[3*i+1], deformed_vertices[3*i+2]])
            # Project vertex in RGBD image
            pix[0] = vertex[0]/vertex[2]; pix[1] = vertex[1]/vertex[2]
            pix = np.dot(self.intrinsic, pix)
            column_index = int(round(pix[0])); line_index = int(round(pix[1]))
            
            min_dist = np.inf
            closest = np.array([0., 0., 0.])
            for i in range(max(0,column_index-s), min(column_index,s+1)):
                for j in range(max(0,line_index-s), min(line_index,s+1)):
                    pt = self.vmap[i,j,:]
                    nme = self.nmap[i,j,:]
                    dist = LA.norm(vertex-pt)
                    if (dist < min_dist):
                        min_dist = dist
                        closest = pt
            res = np.hstack((res, (vertex-closest).flatten()))
        return np.array(res)
            
    def Energy(self, deformed_vertices):
        return np.hstack(np.hstack(self.Smoothness(deformed_vertices), self.Identity(deformed_vertices)), self.Correspondences(deformed_vertices))
    
    def Minimize(self):
        initial_values = self.Mesh.Vertices[:,0:3].flatten()
        
        lower_bound = -np.inf*np.ones((3*len(self.Mesh.Vertices),1))
        upper_bound = np.inf*np.ones((3*len(self.Mesh.Vertices),1))
        
        for i in range(51):
            idx = self.rgb2depth[self.rgbd.shape[i][1], self.rgbd.shape[i][0]]
            lower_bound[3*FACIAL_LANDMARKS[i]:3*FACIAL_LANDMARKS[i]+3] = self.vmap[idx/self.rgbd.Size[1], idx%self.rgbd.Size[1],:] 
            upper_bound[3*FACIAL_LANDMARKS[i]:3*FACIAL_LANDMARKS[i]+3] = self.vmap[idx/self.rgbd.Size[1], idx%self.rgbd.Size[1],:]
        
        least_squares(self.Energy, initial_values,bounds = (lower_bound, upper_bound), verbose = 1)
    
    
    
    
        