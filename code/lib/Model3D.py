#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  2 16:03:42 2017

@author: diegothomas
"""

import imp
from os import path
import numpy as np
from numpy import linalg as LA
from math import sqrt, acos, cos, sin
import cv2
import threading
from multiprocessing import Process
import scipy.sparse as sp
from scipy.sparse.linalg import spsolve
import pyopencl as cl
import pyopencl.array
from pyopencl.elementwise import ElementwiseKernel

APP_ROOT = path.dirname( path.abspath( __file__ ) )
KernelsOpenCL = imp.load_source('BSKernels', APP_ROOT + '/BSKernels.py')

class Model3D():
    
    ''' Constructor '''
    def __init__(self, GPUManager, label_name, weight_name, Size, D2RGB):
        self.path = path
        self.Size = Size
        self.SizeDraw = (480, 640, 3)
        self.GPUManager = GPUManager
        
        img0 = cv2.imread(weight_name, cv2.IMREAD_UNCHANGED)
        img1 = cv2.imread(label_name, cv2.IMREAD_UNCHANGED)
        
        self.Bump = np.zeros(self.Size, dtype = np.float32)
        self.Bump[:,:,2] = np.ascontiguousarray(img1[:,:].astype(np.float32) -1.0)
        self.Bump[img1[:,:] == 0,1] = 0.0
        self.RGB = np.zeros(self.Size, dtype = np.float32)
        self.WeightMap = np.ascontiguousarray(img0.astype(np.float32) /65535.0)
        self.Vertices = np.zeros((28*4325,3), dtype = np.float32)
        self.Normales = np.zeros((28*4325,3), dtype = np.float32)
        self.BlendshapeCoeff = np.zeros(28, dtype = np.float32)
        
        
        self.Bump_Mapping = ElementwiseKernel(self.GPUManager.context, 
                                               """float *Bump, float *RGBMapBump, float *BumpSwap, float *RGBMapBumpSwap, 
                                               float *VMapBump, float *NMapBump, float *VerticesBS, float *BlendshapeCoeff, 
                                               float *VMap, float *NMap, unsigned char *RGBMap, float *Pose, 
                                               float *Pose_D2RGB, float *calib_depth, float *calib_rgb, int n_bump, 
                                               int m_bump, int n_rgbd, int m_rgbd""",
                                               KernelsOpenCL.Kernel_Bump,
                                               "Bump_Mapping")
        
        self.Impaint = ElementwiseKernel(self.GPUManager.context, 
                                               "float *Bump, float *RGBMapBump, float *BumpSwap, float *RGBMapBumpSwap, unsigned char *Label, int nbLines, int nbColumns",
                                               KernelsOpenCL.Kernel_Impaint,
                                               "Impaint")
        
        self.vmap_to_nmap = ElementwiseKernel(self.GPUManager.context, 
                                               "float *vmap, float *nmap, int nbLines, int nbColumns",
                                               KernelsOpenCL.Kernel_NMap,
                                               "vmap_to_nmap")
        
        self.DataProc = ElementwiseKernel(self.GPUManager.context, 
                                               "float *Bump, float *WeightMap, float *Vertices, float *Normales, int *Triangles, float *VerticesBS, int nbLines, int nbColumns",
                                               KernelsOpenCL.Kernel_DataProc,
                                               "DataProc")
        
        self.draw_vmap = ElementwiseKernel(self.GPUManager.context, 
                                               "float *vmap, float *nmap, float *color, unsigned int *res, float *Pose, float *Intrinsic, int color_flag, int nbLines, int nbColumns",
                                               KernelsOpenCL.Kernel_Draw,
                                               "draw_vmap")
        
        self.vmap = ElementwiseKernel(self.GPUManager.context, 
                                               "float *VMapBump, float *NMapBump, float *VerticesBS, float *BlendshapeCoeff, int n_bump, int m_bump",
                                               KernelsOpenCL.Kernel_VMap,
                                               "vmap")
        
        self.vmap_E = ElementwiseKernel(self.GPUManager.context, 
                                               "float *VMapBump, float *NMapBump, float *VerticesBS, float *BlendshapeCoeff, float *Bump, int n_bump, int m_bump",
                                               KernelsOpenCL.Kernel_VMap_E,
                                               "vmap_E")
        
        # Allocate on CPU 
        self.BumpImage_d = cl.array.to_device(self.GPUManager.queue, self.Bump)
        self.WeightMap_d = cl.array.to_device(self.GPUManager.queue, self.WeightMap)
        
        self.VMapBump_d = cl.array.zeros(self.GPUManager.queue, self.Size, np.float32)
        self.NMapBump_d = cl.array.zeros(self.GPUManager.queue, self.Size, np.float32)
        self.RGBBump_d = cl.array.zeros(self.GPUManager.queue, self.Size, np.float32)
        self.RGBSwap_d = cl.array.zeros(self.GPUManager.queue, self.Size, np.float32)
        self.BumpSwap_d = cl.array.zeros(self.GPUManager.queue, self.Size, np.float32)
        self.VerticesBS_d = cl.array.zeros(self.GPUManager.queue, (28*self.Size[0], self.Size[1], 6), np.float32)
        
        self.intrinsic_d = cl.array.zeros(self.GPUManager.queue, 4, np.float32)
        self.intrinsic_RGB_d = cl.array.zeros(self.GPUManager.queue, 4, np.float32)
        self.Pose_d = cl.array.zeros(self.GPUManager.queue, (4,4), np.float32)
        self.Pose_D2RGB_d = cl.array.zeros(self.GPUManager.queue, (4,4), np.float32)
        self.draw_d = cl.array.zeros(self.GPUManager.queue, self.SizeDraw, dtype = np.uint32)
        self.BlendshapeCoeff_d = cl.array.zeros(self.GPUManager.queue, 28, np.float32)
               
        self.Pose_D2RGB_d.set(D2RGB.astype(np.float32))
        
        
    ''' Generate / Update bump image '''
    def BumpImage(self, rgbd, Pose):
        
        self.Pose_d.set(Pose.astype(np.float32))
        intrinsic_depth = np.array([rgbd.intrinsic[0,0], rgbd.intrinsic[1,1], rgbd.intrinsic[0,2], rgbd.intrinsic[1,2]])
        self.intrinsic_d = cl.array.to_device(self.GPUManager.queue, intrinsic_depth)
        intrinsic_rgb = np.array([rgbd.intrinsicRGB[0,0], rgbd.intrinsicRGB[1,1], rgbd.intrinsicRGB[0,2], rgbd.intrinsicRGB[1,2]])
        self.intrinsic_RGB_d = cl.array.to_device(self.GPUManager.queue, intrinsic_rgb)
        
        self.Bump_Mapping(self.BumpImage_d, self.RGBBump_d, self.BumpSwap_d, self.RGBSwap_d, self.VMapBump_d, 
                          self.NMapBump_d, self.VerticesBS_d, self.BlendshapeCoeff_d, 
                          rgbd.vmap_d, rgbd.nmap_d, rgbd.color_d, self.Pose_d, self.Pose_D2RGB_d, 
                          self.intrinsic_d, self.intrinsic_RGB_d, self.Size[0], self.Size[1], self.SizeDraw[0], self.SizeDraw[1])
        
        self.Impaint(self.BumpImage_d, self.RGBBump_d, self.BumpSwap_d, self.RGBSwap_d, self.Size[0], self.Size[1])
        #self.BumpImage_d = self.BumpSwap_d.copy()
        #self.RGBBump_d = self.RGBSwap_d.copy()
        
        self.vmap_to_nmap(self.VMapBump_d, self.NMapBump_d, self.Size[0], self.Size[1])
        
    def PreProcessing(self, BlendShapes):
        for i in range(28):
            self.Vertices[i*4325:(i+1)*4325, ::] = BlendShapes[i].Vertices[::,0:3]
            self.Normales[i*4325:(i+1)*4325, ::] = BlendShapes[i].Normales[::]
        
        self.Vertices_d = cl.array.to_device(self.GPUManager.queue, self.Vertices)
        self.Normales_d = cl.array.to_device(self.GPUManager.queue, self.Normales)
        self.Triangles_d = cl.array.to_device(self.GPUManager.queue, BlendShapes[0].Faces)
        
        self.DataProc(self.BumpImage_d, self.WeightMap_d, self.Vertices_d, self.Normales_d, self.Triangles_d, self.VerticesBS_d, self.Size[0], self.Size[1])
        
    def VMapNMap(self):
        #self.BlendshapeCoeff_d[7] = 1.0
        self.vmap(self.VMapBump_d, self.NMapBump_d, self.VerticesBS_d, self.BlendshapeCoeff_d, self.Size[0], self.Size[1])
        
    
    def VMapExpression(self, Coeff):
        self.BlendshapeCoeff_d.set(Coeff.astype(np.float32))
        self.vmap_E(self.VMapBump_d, self.NMapBump_d, self.VerticesBS_d, self.BlendshapeCoeff_d, self.BumpImage_d, self.Size[0], self.Size[1])
        
    def NMap(self):
        #self.BumpSwap_d = self.BumpImage_d.copy()
        #self.Impaint(self.BumpImage_d, self.RGBBump_d, self.BumpSwap_d, self.RGBSwap_d, self.labels_d, self.Size[0], self.Size[1])
        
        #bump = self.BumpImage_d.get()
        #for i in range(len(self.BridgeList)):
        #    self.BumpImage_d[int(self.BridgeList[i][0][0]), int(self.BridgeList[i][0][1]),0] = 0.8*self.BumpImage_d[int(self.BridgeList[i][0][0]), int(self.BridgeList[i][0][1]),0] + 0.2*self.BumpImage_d[int(self.BridgeList[i][1][0]), int(self.BridgeList[i][1][1]),0]
            
        #self.BumpImage_d.set(bump)
            
        self.vmap_to_nmap(self.VMapBump_d, self.NMapBump_d, self.Size[0], self.Size[1])
        
    def DrawBump(self, Pose, intrinsic, color = 0):
        print "Draw bump image"
        self.Pose_d.set(Pose.astype(np.float32))
        intrinsic_curr = np.array([intrinsic[0,0], intrinsic[1,1], intrinsic[0,2], intrinsic[1,2]])
        self.intrinsic_d = cl.array.to_device(self.GPUManager.queue, intrinsic_curr)
        self.draw_d.set(self.draw_result)
        self.draw_vmap(self.VMapBump_d, self.NMapBump_d, self.RGBBump_d, self.draw_d, self.Pose_d, 
                       self.intrinsic_d, color, self.SizeDraw[0], self.SizeDraw[1])
        return self.draw_d.get()
            
    def Save(self, dest, name):
        print "recording result at: ", dest
        
        vmap = self.VMapBump_d.get()
        nmap = self.NMapBump_d.get()
        rgb = self.RGBBump_d.get()
        bump = self.BumpImage_d.get()
        
        cv2.imwrite(dest+'/Bump_RGB.png',rgb.astype(np.uint8))
        cv2.imwrite(dest+'/Bump.png', 50*(bump[:,:,0]+5.0)*(bump[:,:,1] == 0.0))  

        rgb = np.zeros((self.Size[0], self.Size[1], 3), dtype = np.int32)   
        
        # Write vertices
        Index = np.zeros((self.Size[0], self.Size[1]), dtype = np.int32)
        curr_ind = 0
        Vertices = []
        Normales = []
        Colors = []
        for i in range(self.Size[0]):
            for j in range(self.Size[1]):
                if (vmap[i,j,2] != 0.0):
                    Index[i,j] = curr_ind
                    Vertices.append([vmap[i,j,0], vmap[i,j,1], vmap[i,j,2]])
                    Normales.append([nmap[i,j,0], nmap[i,j,1], nmap[i,j,2]])
                    Colors.append([int(rgb[i,j,0]), int(rgb[i,j,1]), int(rgb[i,j,2])])
                    curr_ind += 1
                
        # Write the faces
        faces = []
        for i in range(self.Size[0]-1):
            for j in range(self.Size[1]-1):
                # triangle 1
                if (vmap[i,j,2] != 0.0 and vmap[i+1,j,2] != 0.0 and vmap[i,j+1,2] != 0.0):
                    faces.append([Index[i,j], Index[i+1,j], Index[i,j+1]])
                #triangle 2
                if (vmap[i,j+1,2] != 0.0 and vmap[i+1,j,2] != 0.0 and vmap[i+1,j+1,2] != 0.0):
                    faces.append([Index[i,j+1], Index[i+1,j], Index[i+1,j+1]])
                   
        
        ''' Write result 3D mesh into a .ply file'''
        f = open(dest+"/"+name+".ply", 'wb')
        
        # Write headers
        f.write("ply\n")
        f.write("format ascii 1.0\n")
        f.write("comment ply file created by Diego Thomas\n")
        f.write("element vertex %d \n" %(len(Vertices)))
        f.write("property float x\n")
        f.write("property float y\n")
        f.write("property float z\n")
        f.write("property float nx\n")
        f.write("property float ny\n")
        f.write("property float nz\n")
        f.write("property uchar red\n")
        f.write("property uchar green\n")
        f.write("property uchar blue\n")
        f.write("element face %d \n" %(len(faces)))
        f.write("property list uchar int vertex_indices\n")
        f.write("end_header\n")
        
        
        for (v,n,r) in zip(Vertices, Normales, Colors):
            f.write("%f %f %f %f %f %f %d %d %d\n" %(v[0], v[1], v[2], 
                                                n[0], n[1], n[2],
                                                r[0], r[1], r[2]))
            
        for face_curr in faces:
            f.write("3 %d %d %d \n" %(face_curr[0], face_curr[1], face_curr[2])) 
        
        f.close()
        