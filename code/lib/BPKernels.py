#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 30 14:03:35 2017

@author: diegothomas
"""

Kernel_BP = """
if (Mask[i] == 0) {
    // Update the messages
    float sigma_val = 1.0f*Sigma[i];
    float factBump = 500.0f;
    float bump_ref_i = Bump[3*i];
    float alpha = 10.0f;
    float alpha_b = 1.0f;
    float w = 0.01f;
    float w_rgb = 0.8f;
    float w_g = 1.0f;
    float beta = 0.001f;
    
    float pt[3];
    pt[0] = VMapBump[3*i];
    pt[1] = VMapBump[3*i+1];
    pt[2] = VMapBump[3*i+2];
    float nmle[3];
    nmle[0] = NMapBump[3*i];
    nmle[1] = NMapBump[3*i+1];
    nmle[2] = NMapBump[3*i+2];
    float rgb_i[3];
    rgb_i[0] = RGBBump[3*i];
    rgb_i[1] = RGBBump[3*i+1];
    rgb_i[2] = RGBBump[3*i+2];
    int Nb_neigbhors_i = Counter[i];
    for (int k=0; k < Nb_neigbhors_i; k++) {
        int j = Edges[i*10 + k];
        float sigma_val_j = 1.0f*Sigma[j];
        float bump_ref_j = Bump[3*j];
        float pt_j[3];
        pt_j[0] = VMapBump[3*j];
        pt_j[1] = VMapBump[3*j+1];
        pt_j[2] = VMapBump[3*j+2];
        float nmle_j[3];
        nmle_j[0] = NMapBump[3*j];
        nmle_j[1] = NMapBump[3*j+1];
        nmle_j[2] = NMapBump[3*j+2];
        float rgb_j[3];
        rgb_j[0] = RGBBump[3*j];
        rgb_j[1] = RGBBump[3*j+1];
        rgb_j[2] = RGBBump[3*j+2];
        
        for (int d_j = 0; d_j < nb_cdtes; d_j++) {
            //Compute new 3D point from d_j
            float dev_j = (bump_ref_j + 10.0f*float(d_j-nb_cdtes/2)/(sigma_val_j*sigma_val_j))/factBump;
            float3 VMapRef_j = {pt_j[0] + (bump_ref_j/factBump)*nmle_j[0], pt_j[1] + (bump_ref_j/factBump)*nmle_j[1], pt_j[2] + (bump_ref_j/factBump)*nmle_j[2]};
            float3 VMapInn_j = {pt_j[0] + dev_j*nmle_j[0], pt_j[1] + dev_j*nmle_j[1], pt_j[2] + dev_j*nmle_j[2]};
    	
            float pt_T_j[3];
            pt_T_j[0] = VMapInn_j.x * Pose[0] + VMapInn_j.y * Pose[1] + VMapInn_j.z * Pose[2] + Pose[3];
            pt_T_j[1] = VMapInn_j.x * Pose[4] + VMapInn_j.y * Pose[5] + VMapInn_j.z * Pose[6] + Pose[7];
            pt_T_j[2] = VMapInn_j.x * Pose[8] + VMapInn_j.y * Pose[9] + VMapInn_j.z * Pose[10] + Pose[11];
            
            //Project onto depth image
            int pix_j[2];
            pix_j[0] = min(m_depth - 1, max(0, convert_int(round((pt_T_j[0] / fabs(pt_T_j[2]))*calib_depth[0] + calib_depth[2]))));
            pix_j[1] = min(n_depth - 1, max(0, convert_int(round((pt_T_j[1] / fabs(pt_T_j[2]))*calib_depth[1] + calib_depth[3]))));
            
            float p1_T_j[3];
            p1_T_j[0] = pt_T_j[0] * Pose_D2RGB[0] + pt_T_j[1] * Pose_D2RGB[1] + pt_T_j[2] * Pose_D2RGB[2] + Pose_D2RGB[3];
            p1_T_j[1] = pt_T_j[0] * Pose_D2RGB[4] + pt_T_j[1] * Pose_D2RGB[5] + pt_T_j[2] * Pose_D2RGB[6] + Pose_D2RGB[7];
            p1_T_j[2] = pt_T_j[0] * Pose_D2RGB[8] + pt_T_j[1] * Pose_D2RGB[9] + pt_T_j[2] * Pose_D2RGB[10] + Pose_D2RGB[11];
            
            //Project onto color image
            int pix_j_rgb[2];
            pix_j_rgb[0] = min(m_rgbd - 1, max(0, convert_int(round((p1_T_j[0] / fabs(p1_T_j[2]))*calib_rgb[0] + calib_rgb[2]))));
            pix_j_rgb[1] = min(n_rgbd - 1, max(0, convert_int(round((p1_T_j[1] / fabs(p1_T_j[2]))*calib_rgb[1] + calib_rgb[3]))));
            
            float3 ptIn_j;
            ptIn_j.x = VMap[3*(pix_j[1]*m_depth + pix_j[0])]; ptIn_j.y = VMap[3*(pix_j[1]*m_depth + pix_j[0]) + 1]; ptIn_j.z = VMap[3*(pix_j[1]*m_depth + pix_j[0]) + 2]; 
            float3 nmleIn_j;
            nmleIn_j.x = NMap[3*(pix_j[1]*m_depth + pix_j[0])]; nmleIn_j.y = NMap[3*(pix_j[1]*m_depth + pix_j[0]) + 1]; nmleIn_j.z = NMap[3*(pix_j[1]*m_depth + pix_j[0]) + 2];    
            float3 color_in_j;
            color_in_j.x = float(rgb[3*(pix_j_rgb[1]*m_rgbd + pix_j_rgb[0])]); 
            color_in_j.y = float(rgb[3*(pix_j_rgb[1]*m_rgbd + pix_j_rgb[0]) + 1]); 
            color_in_j.z = float(rgb[3*(pix_j_rgb[1]*m_rgbd + pix_j_rgb[0]) + 2]); 
        
            float max_mess = 0.0f;
            // Compute max prod
            for (int d_i = 0; d_i < nb_cdtes; d_i++) {
                ///////////// Compute unary potential ///////////////////////
                //Compute new 3D point from d_i
                float dev_i = (bump_ref_i + 10.0f*float(d_i-nb_cdtes/2)/(sigma_val*sigma_val))/factBump;
                float3 VMapRef_i = {pt[0] + (bump_ref_i/factBump)*nmle[0], pt[1] + (bump_ref_i/factBump)*nmle[1], pt[2] + (bump_ref_i/factBump)*nmle[2]};
                float3 VMapInn = {pt[0] + dev_i*nmle[0], pt[1] + dev_i*nmle[1], pt[2] + dev_i*nmle[2]};
    	
                float pt_T[3];
                pt_T[0] = VMapInn.x * Pose[0] + VMapInn.y * Pose[1] + VMapInn.z * Pose[2] + Pose[3];
                pt_T[1] = VMapInn.x * Pose[4] + VMapInn.y * Pose[5] + VMapInn.z * Pose[6] + Pose[7];
                pt_T[2] = VMapInn.x * Pose[8] + VMapInn.y * Pose[9] + VMapInn.z * Pose[10] + Pose[11];
                
                float nmle_T[3];
                nmle_T[0] = nmle[0] * Pose[0] + nmle[1] * Pose[1] + nmle[2] * Pose[2];
                nmle_T[1] = nmle[0] * Pose[4] + nmle[1] * Pose[5] + nmle[2] * Pose[6];
                nmle_T[2] = nmle[0] * Pose[8] + nmle[1] * Pose[9] + nmle[2] * Pose[10];
                
                //Project onto depth image
                int pix[2];
                pix[0] = min(m_depth - 1, max(0, convert_int(round((pt_T[0] / fabs(pt_T[2]))*calib_depth[0] + calib_depth[2]))));
                pix[1] = min(n_depth - 1, max(0, convert_int(round((pt_T[1] / fabs(pt_T[2]))*calib_depth[1] + calib_depth[3]))));
                
                float p1_T[3];
                p1_T[0] = pt_T[0] * Pose_D2RGB[0] + pt_T[1] * Pose_D2RGB[1] + pt_T[2] * Pose_D2RGB[2] + Pose_D2RGB[3];
                p1_T[1] = pt_T[0] * Pose_D2RGB[4] + pt_T[1] * Pose_D2RGB[5] + pt_T[2] * Pose_D2RGB[6] + Pose_D2RGB[7];
                p1_T[2] = pt_T[0] * Pose_D2RGB[8] + pt_T[1] * Pose_D2RGB[9] + pt_T[2] * Pose_D2RGB[10] + Pose_D2RGB[11];
                
                //Project onto color image
                int pix_rgb[2];
                pix_rgb[0] = min(m_rgbd - 1, max(0, convert_int(round((p1_T[0] / fabs(p1_T[2]))*calib_rgb[0] + calib_rgb[2]))));
                pix_rgb[1] = min(n_rgbd - 1, max(0, convert_int(round((p1_T[1] / fabs(p1_T[2]))*calib_rgb[1] + calib_rgb[3]))));
                
                float3 ptIn;
                ptIn.x = VMap[3*(pix[1]*m_depth + pix[0])]; ptIn.y = VMap[3*(pix[1]*m_depth + pix[0]) + 1]; ptIn.z = VMap[3*(pix[1]*m_depth + pix[0]) + 2]; 
                float3 nmleIn;
                nmleIn.x = NMap[3*(pix[1]*m_depth + pix[0])]; nmleIn.y = NMap[3*(pix[1]*m_depth + pix[0]) + 1]; nmleIn.z = NMap[3*(pix[1]*m_depth + pix[0]) + 2]; 
                float3 color_in;
                color_in.x = float(rgb[3*(pix_rgb[1]*m_rgbd + pix_rgb[0])]); 
                color_in.y = float(rgb[3*(pix_rgb[1]*m_rgbd + pix_rgb[0]) + 1]); 
                color_in.z = float(rgb[3*(pix_rgb[1]*m_rgbd + pix_rgb[0]) + 2]); 
                
                float dist_i = sqrt((pt_T[0]-ptIn.x)*(pt_T[0]-ptIn.x) + (pt_T[1]-ptIn.y)*(pt_T[1]-ptIn.y) + (pt_T[2]-ptIn.z)*(pt_T[2]-ptIn.z));
                float dist_angle = nmleIn.x*nmle_T[0] + nmleIn.y*nmle_T[1] + nmleIn.z*nmle_T[2];
                float dist_color = sqrt((rgb_i[0]-color_in.x)*(rgb_i[0]-color_in.x) + 
                                       (rgb_i[1]-color_in.y)*(rgb_i[1]-color_in.y) + 
                                        (rgb_i[2]-color_in.z)*(rgb_i[2]-color_in.z));
                float dist_norm = fabs((ptIn.x-pt_T[0])*nmle_T[0] + (ptIn.y-pt_T[1])*nmle_T[1] + (ptIn.z-pt_T[2])*nmle_T[2]);
                dist_norm = dist_norm/sqrt((ptIn.x-pt_T[0])*(ptIn.x-pt_T[0]) + (ptIn.y-pt_T[1])*(ptIn.y-pt_T[1]) + (ptIn.z-pt_T[2])*(ptIn.z-pt_T[2]));            
                if (dist_norm == 0.0f)
                    dist_norm = 0.001f;
           
                //if (rgb_i[0] == 0.0f && rgb_i[1] == 0.0f && rgb_i[2] == 0.0f)
                //    dist_color = 0.0f;
                    
                    
                float unary;
                alpha = 10.0f*nmle_T[2]*nmle_T[2];
                float fact_unary = alpha*(dist_i + 0.001f/dist_norm) + w*fabs(float((d_i-nb_cdtes/2)))*(sigma_val/20.0f) + beta*dist_color;// 
                float w_e = (sigma_val < 5.0f || ptIn.z > 0.0f) ? 0.001f : 1.0f;
                if (dist_i < 0.05f /*&& dist_angle > 0.0f*/ && nmle_T[2] < 0.2f) {
                    unary = exp(-fact_unary);
                } else {
                    unary = w_e*exp(-w*fabs(float((d_i-nb_cdtes/2)))*(sigma_val/2.0f));
                }
                
                ////////////////////////// Compute Binary potential //////////////////////////////////////
                float VBump[3];
                VBump[0] = pt_T_j[0] - pt_T[0]; VBump[1] = pt_T_j[1] - pt_T[1]; VBump[2] = pt_T_j[2] - pt_T[2]; 
                float VIn[3];
                VIn[0] = ptIn_j.x - ptIn.x; VIn[1] = ptIn_j.y - ptIn.y; VIn[2] = ptIn_j.z - ptIn.z; 
                float dist = sqrt((VBump[0] - VIn[0])*(VBump[0] - VIn[0]) + (VBump[1] - VIn[1])*(VBump[1] - VIn[1]) + (VBump[2] - VIn[2])*(VBump[2] - VIn[2]));
                float normB = sqrt(VBump[0]*VBump[0] + VBump[1]*VBump[1] + VBump[2]*VBump[2]);
                float dist_j = sqrt((pt_T_j[0]-ptIn_j.x)*(pt_T_j[0]-ptIn_j.x) + (pt_T_j[1]-ptIn_j.y)*(pt_T_j[1]-ptIn_j.y) + (pt_T_j[2]-ptIn_j.z)*(pt_T_j[2]-ptIn_j.z));
                float3 ref_v = {(VMapRef_j.x-VMapRef_i.x), (VMapRef_j.y-VMapRef_i.y), (VMapRef_j.z-VMapRef_i.z)};
                float3 dev_v = {(VMapInn_j.x-VMapInn.x), (VMapInn_j.y-VMapInn.y), (VMapInn_j.z-VMapInn.z)};
                //float dist_ref = sqrt((ref_v.x-dev_v.x)*(ref_v.x-dev_v.x) + 
                //                      (ref_v.y-dev_v.y)*(ref_v.y-dev_v.y) + 
                //                      (ref_v.z-dev_v.z)*(ref_v.z-dev_v.z));
                
                float dist_ref = fabs(dev_i - dev_j);
                
                float color_v1[3];
                color_v1[0] = rgb_i[0] - rgb_j[0];
                color_v1[1] = rgb_i[1] - rgb_j[1];
                color_v1[2] = rgb_i[2] - rgb_j[2];
                
                float color_v2[3];
                color_v2[0] = color_in.x - color_in_j.x;
                color_v2[1] = color_in.y - color_in_j.y;
                color_v2[2] = color_in.z - color_in_j.z;
                
                dist_color = sqrt((color_v1[0]-color_v2[0])*(color_v1[0]-color_v2[0]) + 
                                       (color_v1[1]-color_v2[1])*(color_v1[1]-color_v2[1]) + 
                                        (color_v1[2]-color_v2[2])*(color_v1[2]-color_v2[2]));
                
                if ((rgb_i[0] == 0.0f && rgb_i[1] == 0.0f && rgb_i[2] == 0.0f) ||
                (rgb_j[0] == 0.0f && rgb_j[1] == 0.0f && rgb_j[2] == 0.0f))
                    dist_color = 0.0f;
                    
                //if (dist_color > 500)
                //    dist_color = 1.0e3;
                
                //float binary = w*exp(-float((d_j-nb_cdtes/2)*(d_j-nb_cdtes/2))/(sigma_val*sigma_val));
                //if (dist_i < 0.01f && nmle[0] * Pose[8] + nmle[1] * Pose[9] + nmle[2] * Pose[10] < 0.0f &&
                //dist_j < 0.01f && nmle_j[0] * Pose[8] + nmle_j[1] * Pose[9] + nmle_j[2] * Pose[10] < 0.0f)
                //    binary = exp(-alpha_b*dist/normB);// - 1000.0f*dist_ref/normB);
                //float binary = dist_color < 50 ? exp(-1.0f*dist_ref/normB-0.001f*dist_color) : exp(-dist_color);
                //float binary = 1.0f;
                float binary = dist_color < 40 ? exp(-100.0f*dist_ref) : 1.0f; //exp(-1.0f/dist_ref);
                //float binary = exp(-1.0f*dist_ref-0.001f*dist_color);
                
                //Compute product
                float prod_mess = 1.0f;
                for (int v_back = 0; v_back < Nb_neigbhors_i; v_back++) {
                    if (v_back != k) {
                        int back_neigh = Edges[i*10 + v_back];
                        // Search corresponding index in message list
                        int Nb_neigbhors_back= Counter[back_neigh];
                        int indx_i = -1;
                        for (int kk = 0; kk < Nb_neigbhors_back; kk++) {
                            if (Edges[back_neigh*10 + kk] == i) {
                                indx_i = kk;
                                break;
                            }
                        }
                        prod_mess *= (indx_i == -1) ? 1.0f : Message_prev[nb_cdtes*(back_neigh*10 + indx_i) + d_i];
                    }
                }
                if (unary*binary*prod_mess > max_mess)
                    max_mess = unary*binary*prod_mess;
            }
            if (max_mess > 0.0f)
                Message[nb_cdtes*(i*10 + k) + d_j] = kapa*max_mess;
        }
    }
}
"""


Kernel_Normalize = """
if (Mask[i] == 0){
    int Nb_neighbors_i = Counter[i];
    // Normalize the messages
    for (int k=0; k < Nb_neighbors_i; k++) {
        float avg = 0.0f;
        for (int d_j = 0; d_j < nb_cdtes; d_j++) {
            avg += Message[nb_cdtes*(i*10 + k) + d_j];
        }
        
        for (int d_j = 0; d_j < nb_cdtes; d_j++) {
            Message[nb_cdtes*(i*10 + k) + d_j] = Message[nb_cdtes*(i*10 + k) + d_j]/avg;
            Message_prev[nb_cdtes*(i*10 + k) + d_j] = Message[nb_cdtes*(i*10 + k) + d_j];
        }
    }
}
"""

Kernel_Belief = """
if (Mask[i] == 0) {

    float factBump = 500.0f;
    
    //Compute final beliefs
    float sigma_val = 1.0f*Sigma[i];
    int Nb_neigbhors_i = Counter[i];
    float bump_ref = Bump[3*i];
    float alpha = 10.0f;
    float w = 0.01f;
    float w_rgb = 0.8f;
    float w_g = 1.0f;
    float beta = 0.001f;
    
    float pt[3];
    pt[0] = VMapBump[3*i];
    pt[1] = VMapBump[3*i+1];
    pt[2] = VMapBump[3*i+2];
    float nmle[3];
    nmle[0] = NMapBump[3*i];
    nmle[1] = NMapBump[3*i+1];
    nmle[2] = NMapBump[3*i+2];
    float rgb_i[3];
    rgb_i[0] = RGBBump[3*i];
    rgb_i[1] = RGBBump[3*i+1];
    rgb_i[2] = RGBBump[3*i+2];
        
    if (nmle[0] * Pose[8] + nmle[1] * Pose[9] + nmle[2] * Pose[10] < 0.0f) {
        for (int d_i = 0; d_i < nb_cdtes; d_i++) {
            ///////////// Compute unary potential ///////////////////////
            //Compute new 3D point from d_i
            float dev_i = (bump_ref + 10.0f*float(d_i-nb_cdtes/2)/(sigma_val*sigma_val))/factBump;
            float3 VMapInn = {pt[0] + dev_i*nmle[0], pt[1] + dev_i*nmle[1], pt[2] + dev_i*nmle[2]};
        	
            float pt_T[3];
            pt_T[0] = VMapInn.x * Pose[0] + VMapInn.y * Pose[1] + VMapInn.z * Pose[2] + Pose[3];
            pt_T[1] = VMapInn.x * Pose[4] + VMapInn.y * Pose[5] + VMapInn.z * Pose[6] + Pose[7];
            pt_T[2] = VMapInn.x * Pose[8] + VMapInn.y * Pose[9] + VMapInn.z * Pose[10] + Pose[11];
            
            float nmle_T[3];
            nmle_T[0] = nmle[0] * Pose[0] + nmle[1] * Pose[1] + nmle[2] * Pose[2];
            nmle_T[1] = nmle[0] * Pose[4] + nmle[1] * Pose[5] + nmle[2] * Pose[6];
            nmle_T[2] = nmle[0] * Pose[8] + nmle[1] * Pose[9] + nmle[2] * Pose[10];
            
            //Project onto depth image
            int pix[2];
            pix[0] = min(m_depth - 1, max(0, convert_int(round((pt_T[0] / fabs(pt_T[2]))*calib_depth[0] + calib_depth[2]))));
            pix[1] = min(n_depth - 1, max(0, convert_int(round((pt_T[1] / fabs(pt_T[2]))*calib_depth[1] + calib_depth[3]))));
            
            float p1_T[3];
            p1_T[0] = pt_T[0] * Pose_D2RGB[0] + pt_T[1] * Pose_D2RGB[1] + pt_T[2] * Pose_D2RGB[2] + Pose_D2RGB[3];
            p1_T[1] = pt_T[0] * Pose_D2RGB[4] + pt_T[1] * Pose_D2RGB[5] + pt_T[2] * Pose_D2RGB[6] + Pose_D2RGB[7];
            p1_T[2] = pt_T[0] * Pose_D2RGB[8] + pt_T[1] * Pose_D2RGB[9] + pt_T[2] * Pose_D2RGB[10] + Pose_D2RGB[11];
            
            //Project onto color image
            int pix_rgb[2];
            pix_rgb[0] = min(m_rgbd - 1, max(0, convert_int(round((p1_T[0] / fabs(p1_T[2]))*calib_rgb[0] + calib_rgb[2]))));
            pix_rgb[1] = min(n_rgbd - 1, max(0, convert_int(round((p1_T[1] / fabs(p1_T[2]))*calib_rgb[1] + calib_rgb[3]))));
            
            float3 ptIn;
            ptIn.x = VMap[3*(pix[1]*m_depth + pix[0])]; ptIn.y = VMap[3*(pix[1]*m_depth + pix[0]) + 1]; ptIn.z = VMap[3*(pix[1]*m_depth + pix[0]) + 2]; 
            float3 nmleIn;
            nmleIn.x = NMap[3*(pix[1]*m_depth + pix[0])]; nmleIn.y = NMap[3*(pix[1]*m_depth + pix[0]) + 1]; nmleIn.z = NMap[3*(pix[1]*m_depth + pix[0]) + 2]; 
            float3 color_in;
            color_in.x = float(rgb[3*(pix_rgb[1]*m_rgbd + pix_rgb[0])]); 
            color_in.y = float(rgb[3*(pix_rgb[1]*m_rgbd + pix_rgb[0]) + 1]); 
            color_in.z = float(rgb[3*(pix_rgb[1]*m_rgbd + pix_rgb[0]) + 2]); 
            
            float dist = sqrt((pt_T[0]-ptIn.x)*(pt_T[0]-ptIn.x) + (pt_T[1]-ptIn.y)*(pt_T[1]-ptIn.y) + (pt_T[2]-ptIn.z)*(pt_T[2]-ptIn.z));
            float dist_angle = nmleIn.x*nmle_T[0] + nmleIn.y*nmle_T[1] + nmleIn.z*nmle_T[2];
            float dist_color = sqrt((rgb_i[0]-color_in.x)*(rgb_i[0]-color_in.x) + 
                                   (rgb_i[1]-color_in.y)*(rgb_i[1]-color_in.y) + 
                                    (rgb_i[2]-color_in.z)*(rgb_i[2]-color_in.z));
            float dist_norm = fabs((ptIn.x-pt_T[0])*nmle_T[0] + (ptIn.y-pt_T[1])*nmle_T[1] + (ptIn.z-pt_T[2])*nmle_T[2]);
            dist_norm = dist_norm/sqrt((ptIn.x-pt_T[0])*(ptIn.x-pt_T[0]) + (ptIn.y-pt_T[1])*(ptIn.y-pt_T[1]) + (ptIn.z-pt_T[2])*(ptIn.z-pt_T[2]));            
                if (dist_norm == 0.0f)
                    dist_norm = 0.001f;
                    
            //float dist_color = fabs((rgb_i[0]+rgb_i[1]+rgb_i[2])/3.0f -(color_in.x+color_in.y+color_in.z)/3.0f);
            
            
            //if (rgb_i[0] == 0.0f && rgb_i[1] == 0.0f && rgb_i[2] == 0.0f)
             //   dist_color = 0.0f;
            
                
            float unary;
            alpha = 10.0f*nmle_T[2]*nmle_T[2];
            float fact_unary = alpha*(dist + 0.001f/dist_norm) + w*fabs(float((d_i-nb_cdtes/2)))*(sigma_val/20.0f) + beta*dist_color;// 
            float w_e = (sigma_val < 5.0f || ptIn.z > 0.0f) ? 0.001f : 1.0f;
            if (dist > 0.05f /*|| dist_angle < 0.0f*/ ||nmle_T[2] > 0.2f) {
                unary = w_e*exp(-w*fabs(float((d_i-nb_cdtes/2)))*(sigma_val/2.0f));
            } else {
                unary = exp(-fact_unary);
            }
                
            float prod = 1.0f;
            for (int k=0; k < Nb_neigbhors_i; k++) {
                int neigh = Edges[i*10 + k];
                int Nb_neigbhors_back= Counter[neigh];
                // Search corresponding index in message list
                int indx_i = -1;
                for (int kk = 0; kk < Nb_neigbhors_back; kk++) {
                    if (Edges[neigh*10 + kk] == i) {
                        indx_i = kk;
                        break;
                    }
                }
                prod *= (indx_i == -1) ? 1.0f : Message[nb_cdtes*(neigh*10 + indx_i) + d_i];
            }
            Belief[nb_cdtes*i + d_i] = kapa*unary*prod;
        }
    } else {
        for (int d_i = 0; d_i < nb_cdtes; d_i++) 
            Belief[nb_cdtes*i + d_i] = 0.0f;
    }
}
"""

Kernel_NormalizeB = """
if (Mask[i] == 0) {
    // Normalize the beliefs
    float avg = 0.0f;
    for (int d_j = 0; d_j < nb_cdtes; d_j++) {
        avg += Belief[nb_cdtes*i + d_j];
    }
    if (avg == 0.0f)
        avg = 1.0f;
    
    for (int d_j = 0; d_j < nb_cdtes; d_j++) {
        Belief[nb_cdtes*i + d_j] = Belief[nb_cdtes*i + d_j]/avg;
    }
}
"""

Kernel_UpdateBump = """
float factBump = 500.0f;
if (Mask[i] == 0) {

    float3 pt = (float3) {vmap[3*i], vmap[3*i+1], vmap[3*i+2]};
    float3 nmle = (float3) {nmap[3*i], nmap[3*i+1], nmap[3*i+2]};
    if (nmle[0] * Pose[8] + nmle[1] * Pose[9] + nmle[2] * Pose[10] > -0.5f) {
        float d = Bump[3*i]/factBump;
        if (Bump[3*i+1] != 0.0f) {
            vmap[3*i] = pt.x + d*nmle.x; vmap[3*i+1] = pt.y + d*nmle.y; vmap[3*i+2] = pt.z + d*nmle.z;
            if (nmle[0] * Pose[8] + nmle[1] * Pose[9] + nmle[2] * Pose[10] < 0.0f) {
                float pt_T[3];
                pt_T[0] = vmap[3*i] * Pose[0] + vmap[3*i+1] * Pose[1] + vmap[3*i+2] * Pose[2] + Pose[3];
                pt_T[1] = vmap[3*i] * Pose[4] + vmap[3*i+1] * Pose[5] + vmap[3*i+2] * Pose[6] + Pose[7];
                pt_T[2] = vmap[3*i] * Pose[8] + vmap[3*i+1] * Pose[9] + vmap[3*i+2] * Pose[10] + Pose[11];
                
                //Project onto depth image
                int pix[2];
                pix[0] = min(m_depth - 1, max(0, convert_int(round((pt_T[0] / fabs(pt_T[2]))*calib_depth[0] + calib_depth[2]))));
                pix[1] = min(n_depth - 1, max(0, convert_int(round((pt_T[1] / fabs(pt_T[2]))*calib_depth[1] + calib_depth[3]))));
                
                float3 ptIn;
                ptIn.x = VMap[3*(pix[1]*m_depth + pix[0])]; ptIn.y = VMap[3*(pix[1]*m_depth + pix[0]) + 1]; ptIn.z = VMap[3*(pix[1]*m_depth + pix[0]) + 2];
                
                if (ptIn.z != 0.0f && ptIn.z < (pt_T[2] - 0.05f)) { //visibility violation
                   if (Bump[3*i+1] > 0.0f) {
                       Bump[3*i+1] = Bump[3*i+1] -1.0f;
                   }
                   if (Bump[3*i+1] == 0.0f) {
                       vmap[3*i] = 0.0f; vmap[3*i+1] = 0.0f; vmap[3*i+2] = 0.0f;
                       Bump[3*i] = 0.0f;
                       Bump[3*i+1] = 0.0f;
                       Sigma[i] = 1.0f;
                   }
                } /*else {
                    float p1_T[3];
                    p1_T[0] = pt_T[0] * Pose_D2RGB[0] + pt_T[1] * Pose_D2RGB[1] + pt_T[2] * Pose_D2RGB[2] + Pose_D2RGB[3];
                    p1_T[1] = pt_T[0] * Pose_D2RGB[4] + pt_T[1] * Pose_D2RGB[5] + pt_T[2] * Pose_D2RGB[6] + Pose_D2RGB[7];
                    p1_T[2] = pt_T[0] * Pose_D2RGB[8] + pt_T[1] * Pose_D2RGB[9] + pt_T[2] * Pose_D2RGB[10] + Pose_D2RGB[11];
                    
                    //Project onto color image
                    int pix[2];
                    pix[0] = min(m_rgbd - 1, max(0, convert_int(round((p1_T[0] / fabs(p1_T[2]))*calib_rgb[0] + calib_rgb[2]))));
                    pix[1] = min(n_rgbd - 1, max(0, convert_int(round((p1_T[1] / fabs(p1_T[2]))*calib_rgb[1] + calib_rgb[3]))));
                    
                    float3 color_in;
                    color_in.x = float(rgb[3*(pix[1]*m_rgbd + pix[0])]); 
                    color_in.y = float(rgb[3*(pix[1]*m_rgbd + pix[0]) + 1]); 
                    color_in.z = float(rgb[3*(pix[1]*m_rgbd + pix[0]) + 2]); 
                    
                    float dist = sqrt((color_in.x - RGBBump[3*i])*(color_in.x - RGBBump[3*i]) + 
                                (color_in.y - RGBBump[3*i+1])*(color_in.y - RGBBump[3*i+1]) + 
                                (color_in.z - RGBBump[3*i+2])*(color_in.z - RGBBump[3*i+2]));
                    if (dist > 50.0f) {
                        if (Bump[3*i+1] > 0) {
                           Bump[3*i+1] = Bump[3*i+1] -1.0f;
                        }
                        if (Bump[3*i+1] == 0.0f) {
                           vmap[3*i] = 0.0f; vmap[3*i+1] = 0.0f; vmap[3*i+2] = 0.0f;
                           Bump[3*i] = 0.0f;
                           Bump[3*i+1] = 0.0f;
                           Sigma[i] = 1.0f;
                        }
                    }
                }*/
            }
            //Bump[3*i+1] = 1.0f;
        } else {
            vmap[3*i] = 0.0f; vmap[3*i+1] = 0.0f; vmap[3*i+2] = 0.0f;
        }
        //Bump[3*i+1] = 1.0f;
    } else {
        float bump_ref = Bump[3*i];
        float sigma_val = Sigma[i];
        
        float val = 0.0f;
        float max_B = 0.0f;
        float best_d = 0.0f;
        for (int d_j = nb_cdtes/2; d_j < nb_cdtes; d_j++) {
            val += (bump_ref + float(d_j-nb_cdtes/2)/sigma_val)*Belief[nb_cdtes*i + d_j];
            if (Belief[nb_cdtes*i + d_j] > max_B) {
                max_B = Belief[nb_cdtes*i + d_j];
                best_d = (bump_ref + float(d_j-nb_cdtes/2)/sigma_val);
            } 
        }
        for (int d_j = 0; d_j < nb_cdtes/2; d_j++) {
            val += (bump_ref + float(d_j-nb_cdtes/2)/sigma_val)*Belief[nb_cdtes*i + d_j];
            if (Belief[nb_cdtes*i + d_j] > max_B) {
                max_B = Belief[nb_cdtes*i + d_j];
                best_d = (bump_ref + float(d_j-nb_cdtes/2)/sigma_val);
            }
        }
        float w = 0.8f;
        float d = (1.0f-w)*val + w*best_d;
        
        if (max_B > 0.0f) {
            
            if (Bump[3*i+1] != 200.0f) {
                Bump[3*i] = d/factBump > 0.1f ? 0.0f : d;
                Sigma[i] = min(5.0f, Sigma[i] + fabs(nmle[0] * Pose[8] + nmle[1] * Pose[9] + nmle[2] * Pose[10]));
                d = d/factBump;
            } else {
                d = Bump[3*i]/factBump;
            }
            
            vmap[3*i] = pt.x + d*nmle.x; vmap[3*i+1] = pt.y + d*nmle.y; vmap[3*i+2] = pt.z + d*nmle.z;      
            
            float pt_T[3];
            pt_T[0] = vmap[3*i] * Pose[0] + vmap[3*i+1] * Pose[1] + vmap[3*i+2] * Pose[2] + Pose[3];
            pt_T[1] = vmap[3*i] * Pose[4] + vmap[3*i+1] * Pose[5] + vmap[3*i+2] * Pose[6] + Pose[7];
            pt_T[2] = vmap[3*i] * Pose[8] + vmap[3*i+1] * Pose[9] + vmap[3*i+2] * Pose[10] + Pose[11];
            
            float p1_T[3];
            p1_T[0] = pt_T[0] * Pose_D2RGB[0] + pt_T[1] * Pose_D2RGB[1] + pt_T[2] * Pose_D2RGB[2] + Pose_D2RGB[3];
            p1_T[1] = pt_T[0] * Pose_D2RGB[4] + pt_T[1] * Pose_D2RGB[5] + pt_T[2] * Pose_D2RGB[6] + Pose_D2RGB[7];
            p1_T[2] = pt_T[0] * Pose_D2RGB[8] + pt_T[1] * Pose_D2RGB[9] + pt_T[2] * Pose_D2RGB[10] + Pose_D2RGB[11];
            
            //Project onto color image
            int pix[2];
            pix[0] = min(m_rgbd - 1, max(0, convert_int(round((p1_T[0] / fabs(p1_T[2]))*calib_rgb[0] + calib_rgb[2]))));
            pix[1] = min(n_rgbd - 1, max(0, convert_int(round((p1_T[1] / fabs(p1_T[2]))*calib_rgb[1] + calib_rgb[3]))));
            
            float3 color_in;
            color_in.x = float(rgb[3*(pix[1]*m_rgbd + pix[0])]); 
            color_in.y = float(rgb[3*(pix[1]*m_rgbd + pix[0]) + 1]); 
            color_in.z = float(rgb[3*(pix[1]*m_rgbd + pix[0]) + 2]); 
            
            if (Bump[3*i+1] != 200.0f) {
                RGBBump[3*i] = (Bump[3*i+1] *RGBBump[3*i]+color_in.x)/(Bump[3*i+1] +1.0f); 
                RGBBump[3*i+1] = (Bump[3*i+1] *RGBBump[3*i+1]+color_in.y)/(Bump[3*i+1] +1.0f);
                RGBBump[3*i+2] = (Bump[3*i+1] *RGBBump[3*i+2]+color_in.z)/(Bump[3*i+1] +1.0f); 
                
                Bump[3*i+1] = min(100.0f, Bump[3*i+1] + 1.0f);
            }
            
        } else {
            d = Bump[3*i]/factBump;
            if (Bump[3*i+1] != 0.0f) {
                vmap[3*i] = pt.x + d*nmle.x; vmap[3*i+1] = pt.y + d*nmle.y; vmap[3*i+2] = pt.z + d*nmle.z;
                Bump[3*i+1] = 1.0f;
            } else {
                vmap[3*i] = 0.0f; vmap[3*i+1] = 0.0f; vmap[3*i+2] = 0.0f;
                Bump[3*i+1] = 0.0f;
            }
        }
    }
} else if (Mask[i] == 1) {
    float3 pt = (float3) {vmap[3*i], vmap[3*i+1], vmap[3*i+2]};
    float3 nmle = (float3) {nmap[3*i], nmap[3*i+1], nmap[3*i+2]};
    
    float d = Bump[3*i]/factBump;
    Bump[3*i+1] = 0.0f;//-1.0f;
    
    vmap[3*i] = 0.0f; vmap[3*i+1] = 0.0f; vmap[3*i+2] = 0.0f;
    //vmap[3*i] = pt.x + d*nmle.x; vmap[3*i+1] = pt.y + d*nmle.y; vmap[3*i+2] = pt.z + d*nmle.z;
} else {
    Bump[3*i+1] = 0.0f;
}
"""

Kernel_BilateralFilter = """
    if (i%3 == 0) {
        // i is the global index in the Bump image
        int idx_i = (i/3)/nbColumns;    // line index
        int idx_j = (i/3)%nbColumns;    // column index
        
        //if (idx_i > 150 && idx_i < 250 && idx_j > 190 && idx_j < 320) {
        
            if (Bump[i+1] == 1.0f)
                d = 5;
            int max_i = min(idx_i+d+1, nbLines);
            int min_i = max(idx_i-d, 0);
            int max_j = min(idx_j+d+1, nbColumns);
            int min_j = max(idx_j-d, 0);
            
            float res_val = 0.0f;
            float bump_ref = Bump[i];
            float weight = 0.0f;
            float color_ref[3];
            color_ref[0] = RGBBump[i];
            color_ref[1] = RGBBump[i+1];
            color_ref[2] = RGBBump[i+2];
            float color_diff;
            if (Bump[i+1] > 0.0f) {
                for (int k = min_i; k < max_i; k++) {
                    for (int l = min_j; l < max_j; l++) {
                        color_diff = sqrt((color_ref[0]-RGBBump[3*(l + k*nbColumns)])*(color_ref[0]-RGBBump[3*(l + k*nbColumns)]) + 
                                       (color_ref[1]-RGBBump[3*(l + k*nbColumns)+1])*(color_ref[1]-RGBBump[3*(l + k*nbColumns)+1]) + 
                                        (color_ref[2]-RGBBump[3*(l + k*nbColumns)+2])*(color_ref[2]-RGBBump[3*(l + k*nbColumns)+2]));
                        
                        if (Bump[3*(l + k*nbColumns)+1] > 0.0f /*&& fabs(bump_ref - Bump[3*(l + k*nbColumns)]) < 0.1f*/) {
                            if (Bump[3*(l + k*nbColumns)+1] == 10.0f && Bump[i+1] == 1.0f) {
                                res_val = res_val + Bump[3*(l + k*nbColumns)];
                                weight = weight + 1.0f;
                            } else {
                                res_val = res_val + Bump[3*(l + k*nbColumns)]*exp(-(0.3f*pown(fabs(bump_ref - Bump[3*(l + k*nbColumns)])/sigma_r, 2) +
                                                   0.3*pown(sqrt((float)((k-idx_i)*(k-idx_i) + (l-idx_j)*(l-idx_j)))/sigma_d,2) + 
                                                   0.3*pown(color_diff/sigma_c, 2) ));
                                
                                weight = weight + exp(-(0.3f*pown(fabs(bump_ref - Bump[3*(l + k*nbColumns)])/sigma_r, 2) +
                                                    0.3*pown(sqrt((float)((k-idx_i)*(k-idx_i) + (l-idx_j)*(l-idx_j)))/sigma_d,2) + 
                                                    0.3*pown(color_diff/sigma_c, 2) ));
                            }
                        }
                    }
                }
            }
                
            Bump_out[i] = weight > 0.0f ? res_val/weight : bump_ref;
        //}
       // else {
        //    Bump_out[i] = Bump[i];
        //}
    }

"""