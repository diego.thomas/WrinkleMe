#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 30 10:06:26 2017

@author: diegothomas
"""
import imp
from os import path
import numpy as np
import random
import scipy.sparse as sp
import pyopencl as cl
import pyopencl.array
from pyopencl.elementwise import ElementwiseKernel

APP_ROOT = path.dirname( path.abspath( __file__ ) )
KernelsOpenCL = imp.load_source('BPKernels', APP_ROOT + '/BPKernels.py')

NB_CDTES = 20
NB_NEIGHBORS = 4

class BPManager() :
    
    def __init__(self, BSManager, GPUManager, Size):
        self.Size = Size
        self.kapa = 1.0
        self.GPUManager = GPUManager
        
        self.Bump = np.zeros(self.Size, np.float32)
        self.Belief = np.zeros((self.Size[0], self.Size[1], NB_CDTES), np.float32)
        self.Sigma = 0.5*np.ones(self.Size, np.float32)
        
        self.Mask = np.ones(self.Size, np.uint8)*(BSManager.Bump[:,:,1] == -1)+2*np.ones(self.Size, np.uint8)*(BSManager.Bump[:,:,2] == -1.0)
        
        # Build the graph structure
        self.Edges = np.zeros((self.Size[0], self.Size[1], NB_NEIGHBORS), np.int32)
        '''self.availableList = [i*self.Size[1] + j for i in range(self.Size[0]) for j in range(self.Size[1])]
        
        for i in range(self.Size[0]):
            for j in range(self.Size[1]):
                if (self.Mask[i,j]):
                    self.availableList.remove(i*self.Size[1] + j)'''
        
        self.BeliefPropagation = ElementwiseKernel(self.GPUManager.context, 
                                               """float *Sigma, unsigned char *Mask, int *Edges, int *Counter, float *Message_prev, float *Message, 
                                               float *VMapBump, float *NMapBump, float *Bump, float *RGBBump, float *VMap, float *NMap, unsigned char *rgb, 
                                               float *calib_depth, float *calib_rgb, int n_depth, int m_depth, int n_rgbd, int m_rgbd, float *Pose, float *Pose_D2RGB,
                                               float kapa, int Nb_neighbors, int nb_cdtes""",
                                               KernelsOpenCL.Kernel_BP,
                                               "BeliefPropagation")
        
        self.Normalize = ElementwiseKernel(self.GPUManager.context, 
                                               """float *Sigma, unsigned char *Mask, int *Edges, int *Counter, float *Message, float *Message_prev, int Nb_neighbors, int nb_cdtes""",
                                               KernelsOpenCL.Kernel_Normalize,
                                               "Normalize")
        
        self.BeliefUpdate = ElementwiseKernel(self.GPUManager.context, 
                                               """float *Sigma, unsigned char *Mask, int *Edges, int *Counter, float *Message, float *Belief, 
                                               float *VMapBump, float *NMapBump, float *Bump, float *RGBBump, float *VMap, float *NMap, unsigned char *rgb, 
                                               float *calib_depth, float *calib_rgb, int n_depth, int m_depth, int n_rgbd, int m_rgbd, float *Pose, float *Pose_D2RGB, 
                                               float kapa, int Nb_neighbors, int nb_cdtes""",
                                               KernelsOpenCL.Kernel_Belief,
                                               "BeliefUpdate")
        
        self.NormalizeB = ElementwiseKernel(self.GPUManager.context, 
                                               """float *Sigma, unsigned char *Mask, float *Belief, int nb_cdtes""",
                                               KernelsOpenCL.Kernel_NormalizeB,
                                               "NormalizeB")
        
        self.UpdateBump = ElementwiseKernel(self.GPUManager.context, 
                                               """float *Sigma, unsigned char *Mask, float *Belief, float *Bump, float *RGBBump, float *vmap, float *nmap, 
                                               float *VMap, unsigned char *rgb, float *Pose, float *Pose_D2RGB, float *calib_depth, float *calib_rgb, 
                                               int n_depth, int m_depth, int n_rgbd, int m_rgbd, int nb_cdtes""",
                                               KernelsOpenCL.Kernel_UpdateBump,
                                               "UpdateBump")
        
        
        self.BilateralFilter = ElementwiseKernel(self.GPUManager.context, 
                                               "float *Bump, float *Bump_out, float *RGBBump, int d, float sigma_r, float sigma_d, float sigma_c, int nbLines, int nbColumns",
                                               KernelsOpenCL.Kernel_BilateralFilter,
                                               "BilateralFilter")
        
        
        self.Sigma_d = cl.array.to_device(self.GPUManager.queue, self.Sigma)
        self.Mask_d = cl.array.to_device(self.GPUManager.queue, self.Mask.astype(np.uint8))
        self.Bump_buff_d = cl.array.zeros(self.GPUManager.queue, (self.Size[0]*self.Size[1], 3), np.float32)
        self.Edges_d = cl.array.to_device(self.GPUManager.queue, self.Edges)
        self.Belief_d = cl.array.to_device(self.GPUManager.queue, self.Belief)
        self.Messages_d = cl.array.zeros(self.GPUManager.queue, (self.Size[0]*self.Size[1], 10, NB_CDTES), np.float32)
        self.Messages_Prev_d = cl.array.zeros(self.GPUManager.queue, (self.Size[0]*self.Size[1], 10, NB_CDTES), np.float32)
        self.Messages_d.fill(1.0) 
        self.Messages_Prev_d.fill(1.0)                 
        
        self.intrinsic_d = cl.array.zeros(self.GPUManager.queue, 4, np.float32)
        self.intrinsic_RGB_d = cl.array.zeros(self.GPUManager.queue, 4, np.float32)
        self.Pose_d = cl.array.zeros(self.GPUManager.queue, (4,4), np.float32)
        
    
    def Update(self, BS, rgbd, Pose):
        self.Pose_d.set(Pose.astype(np.float32))
        intrinsic_depth = np.array([rgbd.intrinsic[0,0], rgbd.intrinsic[1,1], rgbd.intrinsic[0,2], rgbd.intrinsic[1,2]])
        self.intrinsic_d = cl.array.to_device(self.GPUManager.queue, intrinsic_depth)
        intrinsic_rgb = np.array([rgbd.intrinsicRGB[0,0], rgbd.intrinsicRGB[1,1], rgbd.intrinsicRGB[0,2], rgbd.intrinsicRGB[1,2]])
        self.intrinsic_RGB_d = cl.array.to_device(self.GPUManager.queue, intrinsic_rgb)
        
        print intrinsic_depth
        print intrinsic_rgb
        
        self.Messages_d.fill(1.0) 
        self.Messages_Prev_d.fill(1.0)    
        BS.BumpImage_d = self.Bump_buff_d.copy()
        
        for it in range(3):
            self.BeliefPropagation(self.Sigma_d, self.Mask_d, self.Edges_d, self.Counter_d, self.Messages_Prev_d, self.Messages_d, 
                                   BS.VMapBump_d, BS.NMapBump_d, BS.BumpImage_d, BS.RGBBump_d, rgbd.vmap_d, rgbd.nmap_d, rgbd.color_d, 
                                   self.intrinsic_d, self.intrinsic_RGB_d, rgbd.Size[0], rgbd.Size[1], rgbd.SizeRGB[0], rgbd.SizeRGB[1],
                                   self.Pose_d, BS.Pose_D2RGB_d, self.kapa, NB_NEIGHBORS, NB_CDTES)     
            self.GPUManager.queue.finish()
            self.Normalize(self.Sigma_d, self.Mask_d, self.Edges_d, self.Counter_d, self.Messages_d, self.Messages_Prev_d, NB_NEIGHBORS, NB_CDTES)          
        
        self.BeliefUpdate(self.Sigma_d, self.Mask_d, self.Edges_d, self.Counter_d, self.Messages_d, self.Belief_d,
                                   BS.VMapBump_d, BS.NMapBump_d, BS.BumpImage_d, BS.RGBBump_d, rgbd.vmap_d, rgbd.nmap_d, rgbd.color_d, 
                                   self.intrinsic_d, self.intrinsic_RGB_d, rgbd.Size[0], rgbd.Size[1], rgbd.SizeRGB[0], rgbd.SizeRGB[1], 
                                   self.Pose_d, BS.Pose_D2RGB_d, self.kapa, NB_NEIGHBORS, NB_CDTES)
        self.GPUManager.queue.finish()
        self.NormalizeB(self.Sigma_d, self.Mask_d, self.Belief_d, NB_CDTES) 
        
        self.UpdateBump(self.Sigma_d, self.Mask_d, self.Belief_d, BS.BumpImage_d, BS.RGBBump_d, BS.VMapBump_d, BS.NMapBump_d, rgbd.vmap_d, rgbd.color_d, 
                        self.Pose_d, BS.Pose_D2RGB_d, self.intrinsic_d, self.intrinsic_RGB_d, rgbd.Size[0], rgbd.Size[1], rgbd.SizeRGB[0], rgbd.SizeRGB[1], NB_CDTES)
        
        self.GPUManager.queue.finish()
        
        print "Updated bump"
        
    def Smooth(self, BS):
        
        self.Bump_buff_d = BS.BumpImage_d.copy()
        self.BilateralFilter(self.Bump_buff_d, BS.BumpImage_d, BS.RGBBump_d, 2, 1.0, 1.0, 10.0, self.Size[0], self.Size[1]) # 0.8
        
        self.GPUManager.queue.finish()
        
    def ShuffleGraph(self, BS):
        self.Edges = np.zeros((self.Size[0], self.Size[1], 10), np.int32)
        self.Counter = np.zeros((self.Size[0], self.Size[1]), np.int32)
        
        Bump = BS.BumpImage_d.get()
        
        curr_list = []
        for i in range(self.Size[0]):
            for j in range(self.Size[1]):
                if (Bump[i,j, 1] > 0.0):
                    curr_list.append(i*self.Size[1] + j)
                    
        
        for i in range(self.Size[0]):
            for j in range(self.Size[1]):
                if (Bump[i,j, 1] > 0.0):
                    for k in range(self.Counter[i,j], NB_NEIGHBORS):
                        indx = random.sample(curr_list, 1)
                        self.Edges[i,j,k] = indx[0]
                        self.Counter[i,j] = self.Counter[i,j] + 1
                        line = indx[0]%self.Size[1]
                        column = indx[0]/self.Size[1]
                        if (self.Counter[line,column] < 100):
                            self.Edges[line,column,self.Counter[line,column]] = i*self.Size[1] + j
                            self.Counter[line,column] = self.Counter[line,column]+1
                        else:
                            print "Not enough space in Edges"
            
        self.Edges_d = cl.array.to_device(self.GPUManager.queue, self.Edges)
        self.Counter_d = cl.array.to_device(self.GPUManager.queue, self.Counter)
        
        
    def UniformGraph(self, BS):
        self.Edges = np.zeros((self.Size[0], self.Size[1], 10), np.int32)
        self.Counter = np.zeros((self.Size[0], self.Size[1]), np.int32)
        
        Bump = BS.BumpImage_d.get()
        
        for i in range(self.Size[0]):
            for j in range(self.Size[1]):
                if (Bump[i,j, 1] > 0.0):
                    c = 0
                    for k in range(max(0,i-1), min(i+2)):
                        for l in range(max(0,j-1), min(j+2)):
                            if (Bump[k,l, 1] > 0.0 and (i,j) != (k,l)):
                                self.Edges[i,j,c] = k*self.Size[1] + l
                                self.Counter[i,j] = self.Counter[i,j] + 1
                        
        self.Edges_d = cl.array.to_device(self.GPUManager.queue, self.Edges)
        self.Counter_d = cl.array.to_device(self.GPUManager.queue, self.Counter)
        
        
        